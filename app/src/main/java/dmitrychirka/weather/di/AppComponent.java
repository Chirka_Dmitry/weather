package dmitrychirka.weather.di;

import javax.inject.Singleton;

import dagger.Component;
import dmitrychirka.weather.ui.search.SearchActivity;

@Component(modules = {ContextModule.class, ViewModelModule.class, NetworkModule.class, RoomModule.class})
@Singleton
public interface AppComponent {
    void inject(SearchActivity searchActivity);

    MainActivityComponent plusMainActivityComponent(MainActivityModule mainActivityModule);
}