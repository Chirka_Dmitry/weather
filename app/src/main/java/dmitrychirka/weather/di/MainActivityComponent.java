package dmitrychirka.weather.di;

import dagger.Subcomponent;
import dmitrychirka.weather.di.scopes.ActivityScope;
import dmitrychirka.weather.ui.main.MainActivity;

@Subcomponent(modules = {MainActivityModule.class})
@ActivityScope
public interface MainActivityComponent {

    void inject(MainActivity mainActivity);
}
