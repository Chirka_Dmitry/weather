package dmitrychirka.weather.di;

import androidx.fragment.app.FragmentManager;

import dagger.Module;
import dagger.Provides;
import dmitrychirka.weather.di.scopes.ActivityScope;
import dmitrychirka.weather.ui.main.MainActivity;

@Module
public class MainActivityModule {
    private final MainActivity maContext;

    public MainActivityModule(MainActivity maContext) {
        this.maContext = maContext;
    }

    @Provides
    @ActivityScope
    public MainActivity context() {
        return maContext;
    }

    @Provides
    @ActivityScope
    FragmentManager provideFragmentManager() {
        return maContext.getSupportFragmentManager();
    }

}