package dmitrychirka.weather.di;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import dmitrychirka.weather.model.Repository;
import dmitrychirka.weather.model.db.AppDatabase;
import dmitrychirka.weather.model.network.NetworkApi;
import retrofit2.Retrofit;

@Module
public class RepositoryModule {

    @Singleton
    @Provides
    Repository provideRepository(Context context, NetworkApi networkApi, AppDatabase db) {
        return new Repository(context, networkApi, db);
    }
}
