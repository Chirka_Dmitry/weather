package dmitrychirka.weather.di;

import android.content.Context;
import android.util.Log;

import androidx.room.Room;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import dmitrychirka.weather.model.db.AppDatabase;

@Module
public class RoomModule {

    private String dbName = "app-db.sqlite";

    @Singleton
    @Provides
    AppDatabase getAppDatabase(Context context) {
        copyAttachedDatabase(context, dbName);
        return Room.databaseBuilder(context, AppDatabase.class, dbName)
                .build();
    }

    private void copyAttachedDatabase(Context context, String databaseName) {
        File dbPath = context.getDatabasePath(databaseName);
        // If the database already exists, return
        if (dbPath.exists()) {
            return;
        }

        // Make sure we have a path to the file
        dbPath.getParentFile().mkdirs();

        // Try to copy database file
        try {
            InputStream inputStream = context.getAssets().open(dbName);
            FileOutputStream output = new FileOutputStream(dbPath);

            byte[] buffer = new byte[1024];
            while (inputStream.read(buffer) > 0) {
                output.write(buffer);
                Log.d("#DB", "writing>>");
            }
            output.flush();
            output.close();
            inputStream.close();
            Log.d("#DB", "completed..");
        } catch (IOException e) {
            Log.d("copyAttachedDatabase", "Failed to open file ${e.message}");
            e.printStackTrace();
        }
    }
}
