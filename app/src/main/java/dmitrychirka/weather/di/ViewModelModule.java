package dmitrychirka.weather.di;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;
import dmitrychirka.weather.ui.main.MainViewModel;
import dmitrychirka.weather.ui.search.SearchActivityViewModel;

@Module
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel.class)
    abstract ViewModel bindMainActivityViewModel(MainViewModel mainViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(SearchActivityViewModel.class)
    abstract ViewModel bindSearchActivityViewModel(SearchActivityViewModel searchActivityViewModel);

    @Binds
    abstract ViewModelProvider.Factory bindViewModelFactory(FactoryViewModel factory);
}
