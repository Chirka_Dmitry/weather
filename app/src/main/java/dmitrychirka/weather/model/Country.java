package dmitrychirka.weather.model;

public class Country {

    private String shortName;
    private String longName;
    private int flag;

    public Country(String shortName, String longName, int flag) {
        this.shortName = shortName;
        this.longName = longName;
        this.flag = flag;
    }

    public String getShortName() {
        return shortName;
    }

    public String getLongName() {
        return longName;
    }

    public int getFlag() {
        return flag;
    }
}
