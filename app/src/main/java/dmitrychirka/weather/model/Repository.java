package dmitrychirka.weather.model;

import android.content.Context;

import javax.inject.Inject;
import javax.inject.Singleton;

import dmitrychirka.weather.model.db.AppDatabase;
import dmitrychirka.weather.model.network.NetworkApi;

@Singleton
public class Repository {
    public static final String TAG = "Repository";
    private AppDatabase db;
    private Context context;
    private NetworkApi networkApi;

    @Inject
    public Repository(Context context, NetworkApi networkApi, AppDatabase db) {
        this.context = context;
        this.networkApi = networkApi;
        this.db = db;
    }

    public AppDatabase getAppDatabase() {
        return db;
    }

    public NetworkApi getNetworkApi() {
        return networkApi;
    }

    public Context getContext() {
        return context;
    }

}
