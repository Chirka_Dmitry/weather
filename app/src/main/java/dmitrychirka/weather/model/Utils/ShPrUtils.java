package dmitrychirka.weather.model.Utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import dmitrychirka.weather.R;

public class ShPrUtils {
    private static final String APP_ID = "app_id";
    private static final String ID = "id";
    private static final String COUNTRY_SHORT = "country_short";
    private static final String COUNTRY_LONG = "country_long";
    private static final String LATITUDE = "latitude";
    private static final String LONGITUDE = "longitude";
    public static final String LANGUAGE_KEY = "language";
    public static final String TEMP_SCALE = "temperature_scale";
    private static final String COUNTRY_FLAG = "country_flag";
    private static final String LOCALE_CHANGED = "locale_changed";

    private static SharedPreferences getPref(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    public static void updateCity(Context context, String cityId, String countryShort, String countryLong, String cityLat, String cityLon, Integer countryFlag) {
        SharedPreferences.Editor editor = getPref(context).edit();
        editor
                .putString(ID, cityId)
                .putString(COUNTRY_SHORT, countryShort)
                .putString(COUNTRY_LONG, countryLong)
                .putString(LATITUDE, cityLat)
                .putString(LONGITUDE, cityLon)
                .putInt(COUNTRY_FLAG, countryFlag)
                .apply();
    }

    public static String getCityId(Context context) {
        return getPref(context).getString(ID, "687700");
    }

    public static String getCountryShort(Context context) {
        return getPref(context).getString(COUNTRY_SHORT, "UA");
    }

    public static String getCountryLong(Context context) {
        return getPref(context).getString(COUNTRY_LONG, "Ukraine");
    }

    public static String getCityLatitude(Context context) {
        return getPref(context).getString(LATITUDE, "47.8167");
    }

    public static String getCityLongitude(Context context) {
        return getPref(context).getString(LONGITUDE, "35.1833");
    }

    public static String getAppId(Context context) {
        return getPref(context).getString(APP_ID, "3e29e62e2ddf6dd3d2ebd28aed069215");
    }

    public static void setAppId(Context context, String appId) {
        SharedPreferences.Editor editor = getPref(context).edit();
        editor.putString(APP_ID, appId)
                .apply();
    }

    public static String getLanguagePref(Context context) {
        return getPref(context).getString(LANGUAGE_KEY, LocaleManager.ENGLISH);
    }

    static void setLanguagePref(Context context, String language) {
        SharedPreferences.Editor editor = getPref(context).edit();
        editor.putString(LANGUAGE_KEY, language)
                .apply();
    }

    public static Boolean getLocaleChanged(Context context) {
        return getPref(context).getBoolean(LOCALE_CHANGED, false);
    }

    public static void setLocaleChanged(Context context, Boolean isChanged) {
        SharedPreferences.Editor editor = getPref(context).edit();
        editor.putBoolean(LOCALE_CHANGED, isChanged)
                .apply();
    }

    static String getTempScale(Context context) {
        return getPref(context).getString(TEMP_SCALE, "celsius");
    }

    public static int getCountryFlag(Context context) {
        return getPref(context).getInt(COUNTRY_FLAG, R.drawable.flag_ukraine);
    }
}
