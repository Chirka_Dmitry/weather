package dmitrychirka.weather.model.Utils;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import dmitrychirka.weather.R;
import dmitrychirka.weather.model.Country;

public class Utils {
    public static String getTemperature(Context context, Double tempKelvin) {
        if (tempKelvin == null) {
            return "";
        }
        String typeDegrees = getTypeDegrees(ShPrUtils.getTempScale(context));
        int temp = getTempInScaleDegree(tempKelvin, ShPrUtils.getTempScale(context));

        String result = "";
        if (temp > 0) {
            result = "+" + temp + typeDegrees;
        } else {
            result = temp + typeDegrees;
        }
        return result;
    }

    private static String getTypeDegrees(String tempScale) {
        if ("celsius".equals(tempScale)) {
            return " \u2103";
        } else if ("fahrenheit".equals(tempScale)) {
            return " \u2109";
        }
        return " \u212A";
    }

    private static Integer getTempInScaleDegree(Double tempKelvin, String tempScale) {
        if ("celsius".equals(tempScale)) {
            return (int) Math.round(tempKelvin - 273.15);
        } else if ("fahrenheit".equals(tempScale)) {
            return (int) Math.round((tempKelvin - 273.15) * 9 / 5 + 32);
        }
        return (int) Math.round(tempKelvin);
    }

    public static String getTimeHoursMinutes(Context context, Long time) {
        if (time == null) return null;
        return new SimpleDateFormat("HH:mm", Utils.getLocale(context)).format(time * 1000);
    }

    public static String getDayOfWeekShort(Context context, Long time) {
        return new SimpleDateFormat("EEE", Utils.getLocale(context)).format(time * 1000);
    }

    public static String getDayOfWeekLong(Context context, Long time) {
        return new SimpleDateFormat("EEEE", Utils.getLocale(context)).format(time * 1000);
    }

    public static String getDayAndMonth(Context context, Long time) {
        return new SimpleDateFormat("dd.MM", Utils.getLocale(context)).format(time * 1000);
    }

    public static String getUpdateTimeFromLong(Context context, Long time) {
        return new SimpleDateFormat("dd.MM.yy HH:mm", Utils.getLocale(context)).format(time);
    }

    public static Drawable getWeatherIcon(Context context, String iconDay) {
        switch (iconDay) {
            case "01d":
                return context.getDrawable(R.drawable.clear_sky_d);
            case "01n":
                return context.getDrawable(R.drawable.clear_sky_n);
            case "02d":
                return context.getDrawable(R.drawable.few_clouds_d);
            case "02n":
                return context.getDrawable(R.drawable.few_clouds_n);
            case "03d":
            case "03n":
            case "04d":
            case "04n":
                return context.getDrawable(R.drawable.clouds);
            case "09d":
            case "09n":
                return context.getDrawable(R.drawable.shower_rain);
            case "10d":
                return context.getDrawable(R.drawable.rain_d);
            case "10n":
                return context.getDrawable(R.drawable.rain_n);
            case "11d":
            case "11n":
                return context.getDrawable(R.drawable.thunderstorm);
            case "13d":
                return context.getDrawable(R.drawable.snow_d);
            case "13n":
                return context.getDrawable(R.drawable.snow_n);
            case "50d":
                return context.getDrawable(R.drawable.mist_d);
            case "50n":
                return context.getDrawable(R.drawable.mist_n);
            default:
                return context.getDrawable(R.drawable.no_image_d_n);
        }
    }

    public static Drawable getWindDirectionIcon(Context context, Double degree) {
        if (degree == null) {
            return null;
        }
        int direction = (int) (degree % 360 / 22.5);
        switch (direction) {
            case 1:
            case 2:
                return context.getDrawable(R.drawable.wind_se);
            case 3:
            case 4:
                return context.getDrawable(R.drawable.wind_s);
            case 5:
            case 6:
                return context.getDrawable(R.drawable.wind_sw);
            case 7:
            case 8:
                return context.getDrawable(R.drawable.wind_w);
            case 9:
            case 10:
                return context.getDrawable(R.drawable.wind_nw);
            case 11:
            case 12:
                return context.getDrawable(R.drawable.wind_n);
            case 13:
            case 14:
                return context.getDrawable(R.drawable.wind_ne);
            case 0:
            case 15:
                return context.getDrawable(R.drawable.wind_e);
        }
        return null;
    }

    private static Locale getLocale(Context context) {
        switch (ShPrUtils.getLanguagePref(context)) {
            case "ru":
                return new Locale("ru");
            default:
                return Locale.ENGLISH;
        }
    }

    public static long getTomorrowStartSeconds() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.add(Calendar.DATE, 1);
        return calendar.getTimeInMillis() / 1000;
    }

    public static void showKeyboard(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
    }

    public static List<Country> getCountriesEnglish() {
        List<Country> countries = new ArrayList<>();
        countries.add(new Country("ad", "Andorra", R.drawable.flag_andorra));
        countries.add(new Country("af", "Afghanistan", R.drawable.flag_afghanistan));
        countries.add(new Country("ag", "Antigua and Barbuda", R.drawable.flag_antigua_and_barbuda));
        countries.add(new Country("ai", "Anguilla", R.drawable.flag_anguilla));
        countries.add(new Country("al", "Albania", R.drawable.flag_albania));
        countries.add(new Country("am", "Armenia", R.drawable.flag_armenia));
        countries.add(new Country("ao", "Angola", R.drawable.flag_angola));
        countries.add(new Country("aq", "Antarctica", R.drawable.flag_antarctica));
        countries.add(new Country("ar", "Argentina", R.drawable.flag_argentina));
        countries.add(new Country("at", "Austria", R.drawable.flag_austria));
        countries.add(new Country("au", "Australia", R.drawable.flag_australia));
        countries.add(new Country("aw", "Aruba", R.drawable.flag_aruba));
        countries.add(new Country("az", "Azerbaijan", R.drawable.flag_azerbaijan));
        countries.add(new Country("ba", "Bosnia And Herzegovina", R.drawable.flag_bosnia));
        countries.add(new Country("bb", "Barbados", R.drawable.flag_barbados));
        countries.add(new Country("bd", "Bangladesh", R.drawable.flag_bangladesh));
        countries.add(new Country("be", "Belgium", R.drawable.flag_belgium));
        countries.add(new Country("bf", "Burkina Faso", R.drawable.flag_burkina_faso));
        countries.add(new Country("bg", "Bulgaria", R.drawable.flag_bulgaria));
        countries.add(new Country("bh", "Bahrain", R.drawable.flag_bahrain));
        countries.add(new Country("bi", "Burundi", R.drawable.flag_burundi));
        countries.add(new Country("bj", "Benin", R.drawable.flag_benin));
        countries.add(new Country("bl", "Saint Barthélemy", R.drawable.flag_saint_barthelemy));
        countries.add(new Country("bm", "Bermuda", R.drawable.flag_bermuda));
        countries.add(new Country("bn", "Brunei Darussalam", R.drawable.flag_brunei));
        countries.add(new Country("bo", "Bolivia, Plurinational State Of", R.drawable.flag_bolivia));
        countries.add(new Country("br", "Brazil", R.drawable.flag_brazil));
        countries.add(new Country("bs", "Bahamas", R.drawable.flag_bahamas));
        countries.add(new Country("bt", "Bhutan", R.drawable.flag_bhutan));
        countries.add(new Country("bw", "Botswana", R.drawable.flag_botswana));
        countries.add(new Country("by", "Belarus", R.drawable.flag_belarus));
        countries.add(new Country("bz", "Belize", R.drawable.flag_belize));
        countries.add(new Country("ca", "Canada", R.drawable.flag_canada));
        countries.add(new Country("cc", "Cocos (keeling) Islands", R.drawable.flag_cocos));
        countries.add(new Country("cd", "Congo, The Democratic Republic Of The", R.drawable.flag_democratic_republic_of_the_congo));
        countries.add(new Country("cf", "Central African Republic", R.drawable.flag_central_african_republic));
        countries.add(new Country("cg", "Congo", R.drawable.flag_republic_of_the_congo));
        countries.add(new Country("ch", "Switzerland", R.drawable.flag_switzerland));
        countries.add(new Country("ci", "Côte D'ivoire", R.drawable.flag_cote_divoire));
        countries.add(new Country("ck", "Cook Islands", R.drawable.flag_cook_islands));
        countries.add(new Country("cl", "Chile", R.drawable.flag_chile));
        countries.add(new Country("cm", "Cameroon", R.drawable.flag_cameroon));
        countries.add(new Country("cn", "China", R.drawable.flag_china));
        countries.add(new Country("co", "Colombia", R.drawable.flag_colombia));
        countries.add(new Country("cr", "Costa Rica", R.drawable.flag_costa_rica));
        countries.add(new Country("cu", "Cuba", R.drawable.flag_cuba));
        countries.add(new Country("cv", "Cape Verde", R.drawable.flag_cape_verde));
        countries.add(new Country("cx", "Christmas Island", R.drawable.flag_christmas_island));
        countries.add(new Country("cy", "Cyprus", R.drawable.flag_cyprus));
        countries.add(new Country("cz", "Czech Republic", R.drawable.flag_czech_republic));
        countries.add(new Country("de", "Germany", R.drawable.flag_germany));
        countries.add(new Country("dj", "Djibouti", R.drawable.flag_djibouti));
        countries.add(new Country("dk", "Denmark", R.drawable.flag_denmark));
        countries.add(new Country("dm", "Dominica", R.drawable.flag_dominica));
        countries.add(new Country("do", "Dominican Republic", R.drawable.flag_dominican_republic));
        countries.add(new Country("dz", "Algeria", R.drawable.flag_algeria));
        countries.add(new Country("ec", "Ecuador", R.drawable.flag_ecuador));
        countries.add(new Country("ee", "Estonia", R.drawable.flag_estonia));
        countries.add(new Country("eg", "Egypt", R.drawable.flag_egypt));
        countries.add(new Country("er", "Eritrea", R.drawable.flag_eritrea));
        countries.add(new Country("es", "Spain", R.drawable.flag_spain));
        countries.add(new Country("et", "Ethiopia", R.drawable.flag_ethiopia));
        countries.add(new Country("fi", "Finland", R.drawable.flag_finland));
        countries.add(new Country("fj", "Fiji", R.drawable.flag_fiji));
        countries.add(new Country("fk", "Falkland Islands (malvinas)", R.drawable.flag_falkland_islands));
        countries.add(new Country("fm", "Micronesia, Federated States Of", R.drawable.flag_micronesia));
        countries.add(new Country("fo", "Faroe Islands", R.drawable.flag_faroe_islands));
        countries.add(new Country("fr", "France", R.drawable.flag_france));
        countries.add(new Country("ga", "Gabon", R.drawable.flag_gabon));
        countries.add(new Country("gb", "United Kingdom", R.drawable.flag_united_kingdom));
        countries.add(new Country("gd", "Grenada", R.drawable.flag_grenada));
        countries.add(new Country("ge", "Georgia", R.drawable.flag_georgia));
        countries.add(new Country("gf", "French Guyana", R.drawable.flag_guyane));
        countries.add(new Country("gh", "Ghana", R.drawable.flag_ghana));
        countries.add(new Country("gi", "Gibraltar", R.drawable.flag_gibraltar));
        countries.add(new Country("gl", "Greenland", R.drawable.flag_greenland));
        countries.add(new Country("gm", "Gambia", R.drawable.flag_gambia));
        countries.add(new Country("gn", "Guinea", R.drawable.flag_guinea));
        countries.add(new Country("gq", "Equatorial Guinea", R.drawable.flag_equatorial_guinea));
        countries.add(new Country("gr", "Greece", R.drawable.flag_greece));
        countries.add(new Country("gt", "Guatemala", R.drawable.flag_guatemala));
        countries.add(new Country("gw", "Guinea-bissau", R.drawable.flag_guinea_bissau));
        countries.add(new Country("gy", "Guyana", R.drawable.flag_guyana));
        countries.add(new Country("hk", "Hong Kong", R.drawable.flag_hong_kong));
        countries.add(new Country("hn", "Honduras", R.drawable.flag_honduras));
        countries.add(new Country("hr", "Croatia", R.drawable.flag_croatia));
        countries.add(new Country("ht", "Haiti", R.drawable.flag_haiti));
        countries.add(new Country("hu", "Hungary", R.drawable.flag_hungary));
        countries.add(new Country("id", "Indonesia", R.drawable.flag_indonesia));
        countries.add(new Country("ie", "Ireland", R.drawable.flag_ireland));
        countries.add(new Country("il", "Israel", R.drawable.flag_israel));
        countries.add(new Country("im", "Isle Of Man", R.drawable.flag_isleof_man));
        countries.add(new Country("is", "Iceland", R.drawable.flag_iceland));
        countries.add(new Country("in", "India", R.drawable.flag_india));
        countries.add(new Country("iq", "Iraq", R.drawable.flag_iraq_new));
        countries.add(new Country("ir", "Iran, Islamic Republic Of", R.drawable.flag_iran));
        countries.add(new Country("it", "Italy", R.drawable.flag_italy));
        countries.add(new Country("jm", "Jamaica", R.drawable.flag_jamaica));
        countries.add(new Country("jo", "Jordan", R.drawable.flag_jordan));
        countries.add(new Country("jp", "Japan", R.drawable.flag_japan));
        countries.add(new Country("ke", "Kenya", R.drawable.flag_kenya));
        countries.add(new Country("kg", "Kyrgyzstan", R.drawable.flag_kyrgyzstan));
        countries.add(new Country("kh", "Cambodia", R.drawable.flag_cambodia));
        countries.add(new Country("ki", "Kiribati", R.drawable.flag_kiribati));
        countries.add(new Country("km", "Comoros", R.drawable.flag_comoros));
        countries.add(new Country("kn", "Saint Kitts and Nevis", R.drawable.flag_saint_kitts_and_nevis));
        countries.add(new Country("kp", "North Korea", R.drawable.flag_north_korea));
        countries.add(new Country("kr", "South Korea", R.drawable.flag_south_korea));
        countries.add(new Country("kw", "Kuwait", R.drawable.flag_kuwait));
        countries.add(new Country("ky", "Cayman Islands", R.drawable.flag_cayman_islands));
        countries.add(new Country("kz", "Kazakhstan", R.drawable.flag_kazakhstan));
        countries.add(new Country("la", "Lao People's Democratic Republic", R.drawable.flag_laos));
        countries.add(new Country("lb", "Lebanon", R.drawable.flag_lebanon));
        countries.add(new Country("lc", "Saint Lucia", R.drawable.flag_saint_lucia));
        countries.add(new Country("li", "Liechtenstein", R.drawable.flag_liechtenstein));
        countries.add(new Country("lk", "Sri Lanka", R.drawable.flag_sri_lanka));
        countries.add(new Country("lr", "Liberia", R.drawable.flag_liberia));
        countries.add(new Country("ls", "Lesotho", R.drawable.flag_lesotho));
        countries.add(new Country("lt", "Lithuania", R.drawable.flag_lithuania));
        countries.add(new Country("lu", "Luxembourg", R.drawable.flag_luxembourg));
        countries.add(new Country("lv", "Latvia", R.drawable.flag_latvia));
        countries.add(new Country("ly", "Libya", R.drawable.flag_libya));
        countries.add(new Country("ma", "Morocco", R.drawable.flag_morocco));
        countries.add(new Country("mc", "Monaco", R.drawable.flag_monaco));
        countries.add(new Country("md", "Moldova, Republic Of", R.drawable.flag_moldova));
        countries.add(new Country("me", "Montenegro", R.drawable.flag_of_montenegro));
        countries.add(new Country("mg", "Madagascar", R.drawable.flag_madagascar));
        countries.add(new Country("mh", "Marshall Islands", R.drawable.flag_marshall_islands));
        countries.add(new Country("mk", "Macedonia, The Former Yugoslav Republic Of", R.drawable.flag_macedonia));
        countries.add(new Country("ml", "Mali", R.drawable.flag_mali));
        countries.add(new Country("mm", "Myanmar", R.drawable.flag_myanmar));
        countries.add(new Country("mn", "Mongolia", R.drawable.flag_mongolia));
        countries.add(new Country("mo", "Macao", R.drawable.flag_macao));
        countries.add(new Country("mq", "Martinique", R.drawable.flag_martinique));
        countries.add(new Country("mr", "Mauritania", R.drawable.flag_mauritania));
        countries.add(new Country("ms", "Montserrat", R.drawable.flag_montserrat));
        countries.add(new Country("mt", "Malta", R.drawable.flag_malta));
        countries.add(new Country("mu", "Mauritius", R.drawable.flag_mauritius));
        countries.add(new Country("mv", "Maldives", R.drawable.flag_maldives));
        countries.add(new Country("mw", "Malawi", R.drawable.flag_malawi));
        countries.add(new Country("mx", "Mexico", R.drawable.flag_mexico));
        countries.add(new Country("my", "Malaysia", R.drawable.flag_malaysia));
        countries.add(new Country("mz", "Mozambique", R.drawable.flag_mozambique));
        countries.add(new Country("na", "Namibia", R.drawable.flag_namibia));
        countries.add(new Country("nc", "New Caledonia", R.drawable.flag_new_caledonia));
        countries.add(new Country("ne", "Niger", R.drawable.flag_niger));
        countries.add(new Country("ng", "Nigeria", R.drawable.flag_nigeria));
        countries.add(new Country("ni", "Nicaragua", R.drawable.flag_nicaragua));
        countries.add(new Country("nl", "Netherlands", R.drawable.flag_netherlands));
        countries.add(new Country("no", "Norway", R.drawable.flag_norway));
        countries.add(new Country("np", "Nepal", R.drawable.flag_nepal));
        countries.add(new Country("nr", "Nauru", R.drawable.flag_nauru));
        countries.add(new Country("nu", "Niue", R.drawable.flag_niue));
        countries.add(new Country("nz", "New Zealand", R.drawable.flag_new_zealand));
        countries.add(new Country("om", "Oman", R.drawable.flag_oman));
        countries.add(new Country("pa", "Panama", R.drawable.flag_panama));
        countries.add(new Country("pe", "Peru", R.drawable.flag_peru));
        countries.add(new Country("pf", "French Polynesia", R.drawable.flag_french_polynesia));
        countries.add(new Country("pg", "Papua New Guinea", R.drawable.flag_papua_new_guinea));
        countries.add(new Country("ph", "Philippines", R.drawable.flag_philippines));
        countries.add(new Country("pk", "Pakistan", R.drawable.flag_pakistan));
        countries.add(new Country("pl", "Poland", R.drawable.flag_poland));
        countries.add(new Country("pm", "Saint Pierre And Miquelon", R.drawable.flag_saint_pierre));
        countries.add(new Country("pn", "Pitcairn", R.drawable.flag_pitcairn_islands));
        countries.add(new Country("pr", "Puerto Rico", R.drawable.flag_puerto_rico));
        countries.add(new Country("ps", "Palestine", R.drawable.flag_palestine));
        countries.add(new Country("pt", "Portugal", R.drawable.flag_portugal));
        countries.add(new Country("pw", "Palau", R.drawable.flag_palau));
        countries.add(new Country("py", "Paraguay", R.drawable.flag_paraguay));
        countries.add(new Country("qa", "Qatar", R.drawable.flag_qatar));
        countries.add(new Country("re", "Réunion", R.drawable.flag_martinique));
        countries.add(new Country("ro", "Romania", R.drawable.flag_romania));
        countries.add(new Country("rs", "Serbia", R.drawable.flag_serbia));
        countries.add(new Country("ru", "Russian Federation", R.drawable.flag_russian_federation));
        countries.add(new Country("rw", "Rwanda", R.drawable.flag_rwanda));
        countries.add(new Country("sa", "Saudi Arabia", R.drawable.flag_saudi_arabia));
        countries.add(new Country("sb", "Solomon Islands", R.drawable.flag_soloman_islands));
        countries.add(new Country("sc", "Seychelles", R.drawable.flag_seychelles));
        countries.add(new Country("sd", "Sudan", R.drawable.flag_sudan));
        countries.add(new Country("se", "Sweden", R.drawable.flag_sweden));
        countries.add(new Country("sg", "Singapore", R.drawable.flag_singapore));
        countries.add(new Country("sh", "Saint Helena, Ascension And Tristan Da Cunha", R.drawable.flag_saint_helena));
        countries.add(new Country("si", "Slovenia", R.drawable.flag_slovenia));
        countries.add(new Country("sk", "Slovakia", R.drawable.flag_slovakia));
        countries.add(new Country("sl", "Sierra Leone", R.drawable.flag_sierra_leone));
        countries.add(new Country("sm", "San Marino", R.drawable.flag_san_marino));
        countries.add(new Country("sn", "Senegal", R.drawable.flag_senegal));
        countries.add(new Country("so", "Somalia", R.drawable.flag_somalia));
        countries.add(new Country("sr", "Suriname", R.drawable.flag_suriname));
        countries.add(new Country("st", "Sao Tome And Principe", R.drawable.flag_sao_tome_and_principe));
        countries.add(new Country("sv", "El Salvador", R.drawable.flag_el_salvador));
        countries.add(new Country("sx", "Sint Maarten", R.drawable.flag_sint_maarten));
        countries.add(new Country("sy", "Syrian Arab Republic", R.drawable.flag_syria));
        countries.add(new Country("sz", "Swaziland", R.drawable.flag_swaziland));
        countries.add(new Country("tc", "Turks and Caicos Islands", R.drawable.flag_turks_and_caicos_islands));
        countries.add(new Country("td", "Chad", R.drawable.flag_chad));
        countries.add(new Country("tg", "Togo", R.drawable.flag_togo));
        countries.add(new Country("th", "Thailand", R.drawable.flag_thailand));
        countries.add(new Country("tj", "Tajikistan", R.drawable.flag_tajikistan));
        countries.add(new Country("tk", "Tokelau", R.drawable.flag_tokelau));
        countries.add(new Country("tl", "Timor-leste", R.drawable.flag_timor_leste));
        countries.add(new Country("tm", "Turkmenistan", R.drawable.flag_turkmenistan));
        countries.add(new Country("tn", "Tunisia", R.drawable.flag_tunisia));
        countries.add(new Country("to", "Tonga", R.drawable.flag_tonga));
        countries.add(new Country("tr", "Turkey", R.drawable.flag_turkey));
        countries.add(new Country("tt", "Trinidad &amp; Tobago", R.drawable.flag_trinidad_and_tobago));
        countries.add(new Country("tv", "Tuvalu", R.drawable.flag_tuvalu));
        countries.add(new Country("tw", "Taiwan", R.drawable.flag_taiwan));
        countries.add(new Country("tz", "Tanzania, United Republic Of", R.drawable.flag_tanzania));
        countries.add(new Country("ua", "Ukraine", R.drawable.flag_ukraine));
        countries.add(new Country("ug", "Uganda", R.drawable.flag_uganda));
        countries.add(new Country("ae", "United Arab Emirates (UAE)", R.drawable.flag_uae));
        countries.add(new Country("us", "United States", R.drawable.flag_united_states_of_america));
        countries.add(new Country("uy", "Uruguay", R.drawable.flag_uruguay));
        countries.add(new Country("uz", "Uzbekistan", R.drawable.flag_uzbekistan));
        countries.add(new Country("va", "Holy See (vatican City State)", R.drawable.flag_vatican_city));
        countries.add(new Country("vc", "Saint Vincent &amp; The Grenadines", R.drawable.flag_saint_vicent_and_the_grenadines));
        countries.add(new Country("ve", "Venezuela, Bolivarian Republic Of", R.drawable.flag_venezuela));
        countries.add(new Country("vg", "British Virgin Islands", R.drawable.flag_british_virgin_islands));
        countries.add(new Country("vi", "US Virgin Islands", R.drawable.flag_us_virgin_islands));
        countries.add(new Country("vn", "Vietnam", R.drawable.flag_vietnam));
        countries.add(new Country("vu", "Vanuatu", R.drawable.flag_vanuatu));
        countries.add(new Country("wf", "Wallis And Futuna", R.drawable.flag_wallis_and_futuna));
        countries.add(new Country("ws", "Samoa", R.drawable.flag_samoa));
        countries.add(new Country("ye", "Yemen", R.drawable.flag_yemen));
        countries.add(new Country("yt", "Mayotte", R.drawable.flag_martinique));
        countries.add(new Country("za", "South Africa", R.drawable.flag_south_africa));
        countries.add(new Country("zm", "Zambia", R.drawable.flag_zambia));
        countries.add(new Country("zw", "Zimbabwe", R.drawable.flag_zimbabwe));
        return countries;
    }
}
