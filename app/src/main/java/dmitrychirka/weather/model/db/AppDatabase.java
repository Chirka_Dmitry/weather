package dmitrychirka.weather.model.db;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import javax.inject.Singleton;

import dmitrychirka.weather.model.network.response.cities.City;
import dmitrychirka.weather.model.network.response.weatherForecast.List;
import dmitrychirka.weather.model.network.response.weatherToday.WeatherResponse;

@Singleton
@Database(entities = {City.class, WeatherResponse.class, List.class}, version = 1, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {
    public abstract ListCitiesFromFileDao listCitiesFromFile();

    public abstract CurrentWeatherDao currentWeatherDao();

    public abstract ForecastWeatherDao forecastWeatherDao();
}
