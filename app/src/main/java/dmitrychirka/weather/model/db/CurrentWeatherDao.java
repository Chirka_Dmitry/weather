package dmitrychirka.weather.model.db;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;

import dmitrychirka.weather.model.network.response.weatherToday.WeatherResponse;
import io.reactivex.Flowable;

@Dao
public abstract class CurrentWeatherDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void insert(WeatherResponse weatherResponse);

    @Query("SELECT * from weatherresponse")
    public abstract Flowable<WeatherResponse> getCurrentWeather();

    @Query("DELETE FROM weatherresponse")
    public abstract void deleteCurrWeather();

    @Transaction
    public void updateCurrData(WeatherResponse weatherResponse) {
        deleteCurrWeather();
        insert(weatherResponse);
    }
}
