package dmitrychirka.weather.model.db;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;

import java.util.List;

import io.reactivex.Flowable;


@Dao
public abstract class ForecastWeatherDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void insert(List<dmitrychirka.weather.model.network.response.weatherForecast.List> listForecast);

    @Query("DELETE FROM list")
    public abstract void deleteForecastWeather();

    @Transaction
    public void updateForecastData(List<dmitrychirka.weather.model.network.response.weatherForecast.List> listForecast) {
        deleteForecastWeather();
        insert(listForecast);
    }

    @Query("SELECT * FROM list WHERE mDt >= :startTime AND mDt < :endTime")
    public abstract Flowable<List<dmitrychirka.weather.model.network.response.weatherForecast.List>> getWeatherForPeriod(Long startTime, Long endTime);

    @Query("SELECT * FROM list WHERE mDt >= :startTime")
    public abstract Flowable<List<dmitrychirka.weather.model.network.response.weatherForecast.List>> getForecastWeather(Long startTime);
}
