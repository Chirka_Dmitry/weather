package dmitrychirka.weather.model.db;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

import dmitrychirka.weather.model.network.response.cities.City;
import io.reactivex.Flowable;

@Dao
public interface ListCitiesFromFileDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(List<City> cities);

    @Query("SELECT * FROM city WHERE name LIKE :city || '%' AND country=:country ORDER BY name ASC")
    Flowable<List<City>> getCitiesFromDB(String city, String country);
}
