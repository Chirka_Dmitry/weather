package dmitrychirka.weather.model.network;

import dmitrychirka.weather.model.network.response.weatherForecast.WeatherLongTerm;
import dmitrychirka.weather.model.network.response.weatherToday.WeatherResponse;
import io.reactivex.Single;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface NetworkApi {
    @GET("data/2.5/weather?")
    Single<Response<WeatherResponse>> getWeather(@Query("lat") String lat, @Query("lon") String lon, @Query("APPID") String app_id);

    @GET("data/2.5/forecast?")
    Single<Response<WeatherLongTerm>> getForecastWeather(@Query("id") String id, @Query("lang") String lang, @Query("mode") String mode, @Query("APPID") String app_id);
}