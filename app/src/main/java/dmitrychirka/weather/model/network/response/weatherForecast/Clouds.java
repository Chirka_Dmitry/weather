
package dmitrychirka.weather.model.network.response.weatherForecast;

import androidx.room.Entity;

import com.google.gson.annotations.SerializedName;

@Entity
public class Clouds {

    @SerializedName("all")
    private Long mAll;

    public Long getAll() {
        return mAll;
    }

    public void setAll(Long all) {
        mAll = all;
    }

}
