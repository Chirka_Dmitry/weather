package dmitrychirka.weather.model.network.response.weatherForecast;

import androidx.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

import dmitrychirka.weather.model.network.response.weatherToday.WeatherToday;


public class DataConverter {
    @TypeConverter
    public String fromWeatherList(List<Weather> weatherList) {
        if (weatherList == null) {
            return (null);
        }
        Gson gson = new Gson();
        Type type = new TypeToken<List<Weather>>() {}.getType();
        String json = gson.toJson(weatherList, type);
        return json;
    }

    @TypeConverter
    public List<Weather> toWeatherList(String weatherListString) {
        if (weatherListString == null) {
            return (null);
        }
        Gson gson = new Gson();
        Type type = new TypeToken<List<Weather>>() {}.getType();
        List<Weather> weatherList = gson.fromJson(weatherListString, type);
        return weatherList;
    }
}
