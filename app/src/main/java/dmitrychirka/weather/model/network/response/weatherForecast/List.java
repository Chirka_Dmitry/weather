
package dmitrychirka.weather.model.network.response.weatherForecast;

import androidx.room.Embedded;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.google.gson.annotations.SerializedName;

@Entity
public class List {

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @PrimaryKey(autoGenerate = true)
    private Integer id;

    @SerializedName("clouds")
    @Embedded
    private Clouds mClouds;

    @SerializedName("dt")
    private Long mDt;

    @SerializedName("dt_txt")
    private String mDtTxt;

    @SerializedName("main")
    @Embedded
    private Main mMain;

    @SerializedName("sys")
    @Embedded
    private Sys mSys;

    @SerializedName("weather")
    @TypeConverters(DataConverter.class)
    private java.util.List<Weather> mWeather;

    @Embedded
    @SerializedName("wind")
    private Wind mWind;

    public Clouds getClouds() {
        return mClouds;
    }

    public void setClouds(Clouds clouds) {
        mClouds = clouds;
    }

    public Long getDt() {
        return mDt;
    }

    public void setDt(Long dt) {
        mDt = dt;
    }

    public String getDtTxt() {
        return mDtTxt;
    }

    public void setDtTxt(String dtTxt) {
        mDtTxt = dtTxt;
    }

    public Main getMain() {
        return mMain;
    }

    public void setMain(Main main) {
        mMain = main;
    }

    public Sys getSys() {
        return mSys;
    }

    public void setSys(Sys sys) {
        mSys = sys;
    }

    public java.util.List<Weather> getWeather() {
        return mWeather;
    }

    public void setWeather(java.util.List<Weather> weather) {
        mWeather = weather;
    }

    public Wind getWind() {
        return mWind;
    }

    public void setWind(Wind wind) {
        mWind = wind;
    }

}
