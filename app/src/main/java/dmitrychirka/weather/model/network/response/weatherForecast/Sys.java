
package dmitrychirka.weather.model.network.response.weatherForecast;

import androidx.room.Entity;

import com.google.gson.annotations.SerializedName;

@Entity
public class Sys {

    @SerializedName("pod")
    private String mPod;

    public String getPod() {
        return mPod;
    }

    public void setPod(String pod) {
        mPod = pod;
    }

}
