package dmitrychirka.weather.model.network.response.weatherForecast;

import android.content.Context;
import android.graphics.drawable.Drawable;

import dmitrychirka.weather.model.Utils.Utils;

public class WeatherDay {

    private String dayAndMonth;
    private Long id;
    private Long timeSeconds;
    private Drawable iconDay;
    private String descriptionDay;
    private Double temperatureDay;
    private Drawable iconNight;
    private String descriptionNight;
    private Double temperatureNight;


    public Drawable getIconDay() {
        return iconDay;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getTimeSeconds() {
        return timeSeconds;
    }

    public void setDescriptionDay(String descriptionDay) {
        this.descriptionDay = descriptionDay;
    }

    public String getDescriptionDay() {
        return descriptionDay;
    }

    public void setTemperatureDay(Double temperatureDay) {
        this.temperatureDay = temperatureDay;
    }

    public java.util.List<List> getListWeatherForDay() {
        return listWeatherForDay;
    }

    public void setListWeatherForDay(java.util.List<List> listWeatherForDay) {
        this.listWeatherForDay = listWeatherForDay;
    }

    private java.util.List<List> listWeatherForDay;

    public WeatherDay(Context context, Long timeSeconds) {
        this.timeSeconds = timeSeconds;
        dayAndMonth = Utils.getDayAndMonth(context, timeSeconds);
    }

    public Drawable getIconNight() {
        return iconNight;
    }

    public String getDescriptionNight() {
        return descriptionNight;
    }

    public Double getTemperatureNight() {
        return temperatureNight;
    }

    public void setIconNight(Drawable iconNight) {
        this.iconNight = iconNight;
    }

    public void setDescriptionNight(String descriptionNight) {
        this.descriptionNight = descriptionNight;
    }

    public void setTemperatureNight(Double temperatureNight) {
        this.temperatureNight = temperatureNight;
    }

    public String getDayAndMonth() {
        return dayAndMonth;
    }

    public void setIconDay(Drawable weatherIcon) {
        iconDay = weatherIcon;
    }

    public Double getTemperatureDay() {
        return temperatureDay;
    }

}
