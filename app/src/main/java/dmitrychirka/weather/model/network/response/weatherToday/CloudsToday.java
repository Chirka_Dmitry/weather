
package dmitrychirka.weather.model.network.response.weatherToday;

import androidx.room.Entity;

import com.google.gson.annotations.SerializedName;


@Entity
public class CloudsToday {

    @SerializedName("all")
    private Long mAll;

    public Long getAll() {
        return mAll;
    }

    public void setAll(Long all) {
        mAll = all;
    }

}
