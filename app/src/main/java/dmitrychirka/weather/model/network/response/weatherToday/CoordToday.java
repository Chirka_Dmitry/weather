
package dmitrychirka.weather.model.network.response.weatherToday;

import androidx.room.Entity;

import com.google.gson.annotations.SerializedName;


@Entity
public class CoordToday {

    @SerializedName("lat")
    private Double mLat;
    @SerializedName("lon")
    private Double mLon;

    public Double getLat() {
        return mLat;
    }

    public void setLat(Double lat) {
        mLat = lat;
    }

    public Double getLon() {
        return mLon;
    }

    public void setLon(Double lon) {
        mLon = lon;
    }

}
