package dmitrychirka.weather.model.network.response.weatherToday;

import androidx.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;


public class DataConverter {
    @TypeConverter
    public String fromCountryLangList(List<WeatherToday> countryLang) {
        if (countryLang == null) {
            return (null);
        }
        Gson gson = new Gson();
        Type type = new TypeToken<List<WeatherToday>>() {
        }.getType();
        String json = gson.toJson(countryLang, type);
        return json;
    }

    @TypeConverter
    public List<WeatherToday> toCountryLangList(String countryLangString) {
        if (countryLangString == null) {
            return (null);
        }
        Gson gson = new Gson();
        Type type = new TypeToken<List<WeatherToday>>() {
        }.getType();
        List<WeatherToday> countryLangList = gson.fromJson(countryLangString, type);
        return countryLangList;
    }
}
