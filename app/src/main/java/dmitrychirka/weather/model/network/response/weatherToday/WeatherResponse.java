
package dmitrychirka.weather.model.network.response.weatherToday;

import androidx.room.ColumnInfo;
import androidx.room.Embedded;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.google.gson.annotations.SerializedName;

import java.util.List;

@Entity
public class WeatherResponse{

    @SerializedName("base")
    private String mBase;

    @SerializedName("clouds")
    @Embedded
    private CloudsToday mClouds;

    @SerializedName("cod")
    private Long mCod;

    @SerializedName("coord")
    @Embedded
    private CoordToday mCoord;

    @SerializedName("dt")
    private Long mDt;

    @PrimaryKey
    @SerializedName("id")
    @ColumnInfo(name = "id")
    private Long mId;

    @SerializedName("main")
    @Embedded
    private MainToday mMain;

    @SerializedName("name")
    private String mName;

    @SerializedName("sys")
    @Embedded
    private SysToday mSys;

    @SerializedName("visibility")
    private Long mVisibility;

    @SerializedName("weather")
    @TypeConverters(DataConverter.class)
    private List<WeatherToday> mWeather;

    @Embedded
    @SerializedName("wind")
    private WindToday mWind;

    private Long timeOfUpdate;

    public Long getTimeOfUpdate() {
        return timeOfUpdate;
    }

    public void setTimeOfUpdate(Long timeOfUpdate) {
        this.timeOfUpdate = timeOfUpdate;
    }


    public String getBase() {
        return mBase;
    }

    public void setBase(String base) {
        mBase = base;
    }

    public Long getCod() {
        return mCod;
    }

    public void setCod(Long cod) {
        mCod = cod;
    }

    public void setClouds(CloudsToday mCloudsToday) {
        this.mClouds = mCloudsToday;
    }

    public void setCoord(CoordToday mCoordToday) {
        this.mCoord = mCoordToday;
    }

    public CloudsToday getClouds() {
        return mClouds;
    }

    public CoordToday getCoord() {
        return mCoord;
    }

    public Long getDt() {
        return mDt;
    }

    public void setDt(Long dt) {
        mDt = dt;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public MainToday getMain() {
        return mMain;
    }

    public void setMain(MainToday main) {
        mMain = main;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public SysToday getSys() {
        return mSys;
    }

    public void setSys(SysToday sys) {
        mSys = sys;
    }

    public Long getVisibility() {
        return mVisibility;
    }

    public void setVisibility(Long visibility) {
        mVisibility = visibility;
    }

    public List<WeatherToday> getWeather() {
        return mWeather;
    }

    public void setWeather(List<WeatherToday> weather) {
        mWeather = weather;
    }

    public WindToday getWind() {
        return mWind;
    }

    public void setWind(WindToday wind) {
        mWind = wind;
    }

}
