
package dmitrychirka.weather.model.network.response.weatherToday;

import androidx.room.Entity;

import com.google.gson.annotations.SerializedName;


@Entity
public class WindToday {

    @SerializedName("deg")
    private Double mDeg;
    @SerializedName("speed")
    private Double mSpeed;

    public Double getDeg() {
        return mDeg;
    }

    public void setDeg(Double deg) {
        mDeg = deg;
    }

    public Double getSpeed() {
        return mSpeed;
    }

    public void setSpeed(Double speed) {
        mSpeed = speed;
    }

}
