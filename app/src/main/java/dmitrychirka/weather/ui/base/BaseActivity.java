package dmitrychirka.weather.ui.base;

import android.content.Context;

import androidx.appcompat.app.AppCompatActivity;

import dmitrychirka.weather.model.Utils.LocaleManager;

public abstract class BaseActivity extends AppCompatActivity {

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleManager.setLocale(base));
    }
}
