package dmitrychirka.weather.ui.base;

import androidx.lifecycle.ViewModel;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

public class BaseViewModel extends ViewModel {

    private CompositeDisposable disposable;

    protected BaseViewModel() {
        disposable = new CompositeDisposable();
    }

    protected CompositeDisposable getDisposable() {
        return disposable;
    }

    protected void clearDisposable(Disposable disposable) {
        if (disposable != null && !disposable.isDisposed())
            disposable.dispose();
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        clearDisposable(disposable);
    }
}
