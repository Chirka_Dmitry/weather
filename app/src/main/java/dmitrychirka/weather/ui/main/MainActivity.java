package dmitrychirka.weather.ui.main;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.core.view.GravityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.lifecycle.ViewModelProviders;

import com.facebook.stetho.Stetho;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;

import javax.inject.Inject;

import dmitrychirka.weather.BuildConfig;
import dmitrychirka.weather.R;
import dmitrychirka.weather.databinding.ActivityMainBinding;
import dmitrychirka.weather.di.App;
import dmitrychirka.weather.di.FactoryViewModel;
import dmitrychirka.weather.di.MainActivityModule;
import dmitrychirka.weather.model.Utils.LocaleManager;
import dmitrychirka.weather.model.Utils.ShPrUtils;
import dmitrychirka.weather.model.network.NetworkApi;
import dmitrychirka.weather.ui.base.BaseActivity;
import dmitrychirka.weather.ui.main.forecast.ForecastWeatherFragment;
import dmitrychirka.weather.ui.main.today.TodayWeatherFragment;
import dmitrychirka.weather.ui.main.tomorrow.TomorrowWeatherFragment;
import dmitrychirka.weather.ui.search.SearchActivity;
import dmitrychirka.weather.ui.settings.SettingsActivity;


public class MainActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener {

    View updateButtonView;
    MainViewModel viewModel;
    ActivityMainBinding binding;
    MenuItem updateButton;
    public static final int REQUEST_CODE = 101;
    SharedPreferences.OnSharedPreferenceChangeListener listener;
    SharedPreferences prefs;

    @Inject
    FactoryViewModel viewModelFactory;

    @Inject
    NetworkApi networkApi;

    public MainViewModel getViewModel() {
        return viewModel;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (BuildConfig.DEBUG) {
            Stetho.initialize(
                    Stetho.newInitializerBuilder(getApplication())
                            .enableDumpapp(Stetho.defaultDumperPluginsProvider(getApplication()))
                            .enableWebKitInspector(Stetho.defaultInspectorModulesProvider(getApplication()))
                            .build());
        }
        ((App) getApplication()).getAppComponent().plusMainActivityComponent(new MainActivityModule(this)).inject(this);
        viewModel = ViewModelProviders.of(this, viewModelFactory)
                .get(MainViewModel.class);

        if (savedInstanceState == null) {
            viewModel.getWeatherFromDB();
            viewModel.requestWeather();
        }
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        binding.setContext(this);
        viewModel.getTemp().observe(this, temp -> binding.setTemp(temp));
        viewModel.getHumidity().observe(this, humidity -> binding.setHumidity(humidity));
        viewModel.getPressure().observe(this, binding::setPressure);
        viewModel.getWeatherIcon().observe(this, binding::setWeatherIcon);
        viewModel.getDescription().observe(this, binding::setDescription);
        viewModel.getSpeed().observe(this, binding::setWindSpeed);
        viewModel.getWindDirectIcon().observe(this, binding::setWindDirectIcon);
        viewModel.getSunrise().observe(this, binding::setSunrise);
        viewModel.getSunset().observe(this, binding::setSunset);
        viewModel.getUpdateTime().observe(this, time -> binding.setUpdateTime(time));
        viewModel.getInfoMessage().observe(this, message -> {
            if (!TextUtils.isEmpty(message)) {
                showSnackBar(message);
                viewModel.getInfoMessage().postValue(null);
            }
        });
        viewModel.getUpdateDataBinding().observe(this, isUpdate -> {
            if (isUpdate) {
                binding.invalidateAll();
                viewModel.getUpdateDataBinding().postValue(false);
            }
        });
        initActionBar();
        initDrawer();
        initViewPager();
        initSharedPreferencesListener();
    }

    private void initActionBar() {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(R.string.app_name);
            viewModel.getCityAndCountry().observe(this, name -> getSupportActionBar()
                    .setTitle(!TextUtils.isEmpty(name) ? name : ""));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(isDrawerOpen() ? R.drawable.ic_arrow_back_black_24dp : R.drawable.ic_menu_white_24dp);
        }
    }

    private void initDrawer() {
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, binding.drawerLayout, binding.toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);

        binding.drawerLayout.addDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(@NonNull View drawerView, float slideOffset) {
            }

            @Override
            public void onDrawerOpened(@NonNull View drawerView) {
                if (getSupportActionBar() != null) {
                    getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp);
                }
            }

            @Override
            public void onDrawerClosed(@NonNull View drawerView) {
                if (getSupportActionBar() != null) {
                    getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_menu_white_24dp);
                }
            }

            @Override
            public void onDrawerStateChanged(int newState) {
            }
        });

        toggle.setDrawerIndicatorEnabled(false);
        toggle.syncState();

        binding.navView.setNavigationItemSelectedListener(this);
    }

    private void initViewPager() {
        binding.viewPager.setOffscreenPageLimit(2);
        PagerAdapter adapter = new PagerAdapter(getSupportFragmentManager());
        adapter.addFragment(TodayWeatherFragment.startFragment(), getResources().getString(R.string.name_tab1));
        adapter.addFragment(TomorrowWeatherFragment.startFragment(), getResources().getString(R.string.name_tab2));
        adapter.addFragment(ForecastWeatherFragment.startFragment(), getResources().getString(R.string.name_tab3));
        binding.viewPager.setAdapter(adapter);
        binding.tabLayout.setupWithViewPager(binding.viewPager);
        binding.executePendingBindings();
    }

    private void initSharedPreferencesListener() {
        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        listener = (sharedPreferences, key) -> {
            if (key.equals(ShPrUtils.TEMP_SCALE)) {
                viewModel.getUpdateDataBinding().postValue(true);
            }
        };
        prefs.registerOnSharedPreferenceChangeListener(listener);
    }

    private boolean isDrawerOpen() {
        return binding.drawerLayout.isDrawerOpen(Gravity.LEFT);
    }

    private void openDrawer() {
        binding.drawerLayout.openDrawer(GravityCompat.START);
    }

    private void closeDrawer() {
        binding.drawerLayout.closeDrawer(GravityCompat.START);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        updateButton = menu.findItem(R.id.action_refresh);
        updateButton.setActionView(R.layout.iv_refresh);
        updateButtonView = updateButton.getActionView();
        updateButtonView.setOnClickListener(v -> onOptionsItemSelected(updateButton));
        viewModel.getIsUpdating().observe(this, isUpdating -> {
            if (isUpdating) {
                startAnimation();
            } else {
                stopAnimation();
            }
        });
        return super.onCreateOptionsMenu(menu);
    }

    private void startAnimation() {
        Animation rotation = AnimationUtils.loadAnimation(this, R.anim.rotate_refresh);
        rotation.setRepeatCount(Animation.INFINITE);
        updateButtonView.startAnimation(rotation);
    }

    private void stopAnimation() {
        updateButtonView.clearAnimation();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (ShPrUtils.getLocaleChanged(this)) {
            ShPrUtils.setLocaleChanged(this, false);
            setNewLocale(ShPrUtils.getLanguagePref(this));
        }
    }

    @Override
    public void onBackPressed() {
        if (isDrawerOpen()) {
            closeDrawer();
            return;
        }
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_refresh:
                viewModel.requestWeather();
                break;

            case R.id.action_search:
                startSearchActivity();
                break;

            case android.R.id.home:
                if (isDrawerOpen()) {
                    closeDrawer();
                } else {
                    openDrawer();
                }
                break;
        }
        return true;
    }

    public void startSearchActivity() {
        Intent intent = new Intent(this, SearchActivity.class);
        startActivityForResult(intent, REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == REQUEST_CODE) {
            viewModel.requestWeather();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_settings:
                startActivity(new Intent(this, SettingsActivity.class));
                break;
            case R.id.nav_about:
                viewModel.getInfoMessage().postValue(getResources().getString(R.string.about));
                break;
            case R.id.nav_developer:
                viewModel.getInfoMessage().postValue(getResources().getString(R.string.developer));
                break;
        }
        return true;
    }

    private void setNewLocale(@LocaleManager.LocaleDef String language) {
        LocaleManager.setNewLocale(this, language);
        finish();
        overridePendingTransition(0, 0);
        startActivity(getIntent());
        overridePendingTransition(0, 0);
    }

    private void showSnackBar(String message) {
        Snackbar snackbar = Snackbar.make(binding.getRoot(), message, Snackbar.LENGTH_LONG);
        snackbar.setAnimationMode(Snackbar.ANIMATION_MODE_SLIDE);
        View sbView = snackbar.getView();
        sbView.setBackgroundColor(getResources().getColor(R.color.colorTintedBackgroundDark));
        TextView textView = sbView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setGravity(Gravity.CENTER_HORIZONTAL);
        textView.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        textView.setTextColor(getResources().getColor(R.color.colorBackgroundDefault));
        textView.setTextSize(18);
        textView.setMaxLines(5);
        snackbar.show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        prefs.unregisterOnSharedPreferenceChangeListener(listener);
    }
}