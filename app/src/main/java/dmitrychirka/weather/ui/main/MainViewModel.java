package dmitrychirka.weather.ui.main;

import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;

import javax.inject.Inject;

import dmitrychirka.weather.R;
import dmitrychirka.weather.model.Repository;
import dmitrychirka.weather.model.Utils.ShPrUtils;
import dmitrychirka.weather.model.Utils.Utils;
import dmitrychirka.weather.model.network.response.cities.City;
import dmitrychirka.weather.model.network.response.weatherForecast.List;
import dmitrychirka.weather.model.network.response.weatherForecast.WeatherDay;
import dmitrychirka.weather.model.network.response.weatherToday.WeatherResponse;
import dmitrychirka.weather.ui.base.BaseViewModel;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class MainViewModel extends BaseViewModel {

    private static final Long NUMBER_FOR_ID = 100_000_000L;
    private static final int CURRENT_DAY = 0;
    private static final int TOMORROW = 1;
    private static final int AFTER_TOMORROW = 2;
    private Repository repository;
    private WeatherResponse wrNow;
    private java.util.List<List> wrLong;
    private Disposable disposableCurrWeather;
    private Disposable disposableForecastWeather;

    private MutableLiveData<Double> temp = new MutableLiveData<>();
    private MutableLiveData<Long> humidity = new MutableLiveData<>();
    private MutableLiveData<Double> pressure = new MutableLiveData<>();
    private MutableLiveData<Drawable> weatherIcon = new MutableLiveData<>();
    private MutableLiveData<String> description = new MutableLiveData<>();
    private MutableLiveData<Drawable> windDirectIcon = new MutableLiveData<>();
    private MutableLiveData<Double> speed = new MutableLiveData<>();
    private MutableLiveData<String> sunrise = new MutableLiveData<>();
    private MutableLiveData<String> sunset = new MutableLiveData<>();
    private MutableLiveData<String> updateTime = new MutableLiveData<>();
    private MutableLiveData<String> cityAndCountry = new MutableLiveData<>();
    private MutableLiveData<java.util.List<List>> todayWeather = new MutableLiveData<>();
    private MutableLiveData<java.util.List<List>> tomorrowWeather = new MutableLiveData<>();
    private MutableLiveData<java.util.List<WeatherDay>> forecastWeather = new MutableLiveData<>();
    private MutableLiveData<Boolean> isUpdating = new MutableLiveData<>();
    private MutableLiveData<Boolean> updateDataBinding = new MutableLiveData<>();

    @Inject
    MainViewModel(Repository repository) {
        this.repository = repository;
    }


    public MutableLiveData<Boolean> getUpdateDataBinding() {
        return updateDataBinding;
    }

    public MutableLiveData<Boolean> getIsUpdating() {
        return isUpdating;
    }

    private MutableLiveData<String> infoMessage = new MutableLiveData<>();

    public MutableLiveData<String> getUpdateTime() {
        return updateTime;
    }

    public MutableLiveData<String> getCityAndCountry() {
        return cityAndCountry;
    }

    public MutableLiveData<java.util.List<List>> getTodayWeather() {
        return todayWeather;
    }

    public MutableLiveData<java.util.List<WeatherDay>> getForecastWeather() {
        return forecastWeather;
    }

    public MutableLiveData<String> getInfoMessage() {
        return infoMessage;
    }

    public MutableLiveData<java.util.List<List>> getTomorrowWeather() {
        return tomorrowWeather;
    }

    public MutableLiveData<Double> getTemp() {
        return temp;
    }

    public MutableLiveData<Long> getHumidity() {
        return humidity;
    }

    public MutableLiveData<Double> getPressure() {
        return pressure;
    }

    public MutableLiveData<Drawable> getWeatherIcon() {
        return weatherIcon;
    }

    public MutableLiveData<String> getDescription() {
        return description;
    }

    public MutableLiveData<Drawable> getWindDirectIcon() {
        return windDirectIcon;
    }

    public MutableLiveData<Double> getSpeed() {
        return speed;
    }

    public MutableLiveData<String> getSunrise() {
        return sunrise;
    }

    public MutableLiveData<String> getSunset() {
        return sunset;
    }

    void requestWeather() {
        isUpdating.postValue(true);
        clearWeatherResults();
        requestCurrentWeather();
        requestForecastWeather();
    }

    private void clearWeatherResults() {
        wrNow = null;
        wrLong = null;
    }

    private void requestCurrentWeather() {
        clearDisposable(disposableCurrWeather);
        disposableCurrWeather = repository.getNetworkApi()
                .getWeather(ShPrUtils.getCityLatitude(repository.getContext()),
                        ShPrUtils.getCityLongitude(repository.getContext()),
                        ShPrUtils.getAppId(repository.getContext()))
                .subscribeOn(Schedulers.io())
                .subscribe(
                        response -> {
                            if (response.isSuccessful() && response.body() != null) {
                                wrNow = response.body();
                                wrNow.setTimeOfUpdate(System.currentTimeMillis());
                                writeWeatherToDB();
                            } else if (response.errorBody() != null) {
                                isUpdating.postValue(false);
                                Log.d("myLogs", "Error from server");
                            }
                        },
                        e -> {
                            infoMessage.postValue(repository.getContext().getResources().getString(R.string.error_request));
                            isUpdating.postValue(false);
                            e.printStackTrace();
                        });
        getDisposable().add(disposableCurrWeather);
    }

    private void requestForecastWeather() {
        clearDisposable(disposableForecastWeather);
        disposableForecastWeather = repository.getNetworkApi()
                .getForecastWeather(ShPrUtils.getCityId(repository.getContext()),
                        ShPrUtils.getLanguagePref(repository.getContext()), "json",
                        ShPrUtils.getAppId(repository.getContext()))
                .subscribeOn(Schedulers.io())
                .subscribe(
                        response -> {
                            if (response.isSuccessful() && response.body() != null) {
                                wrLong = response.body().getList();
                                writeWeatherToDB();
                            } else if (response.errorBody() != null) {
                                isUpdating.postValue(false);
                            }
                        },
                        e -> {
                            infoMessage.postValue(repository.getContext().getResources().getString(R.string.error_request));
                            isUpdating.postValue(false);
                            e.printStackTrace();
                        });
        getDisposable().add(disposableForecastWeather);
    }

    private void writeWeatherToDB() {
        if (wrNow != null && wrLong != null) {
            repository.getAppDatabase().runInTransaction(() -> {
                repository.getAppDatabase().currentWeatherDao().updateCurrData(wrNow);
                repository.getAppDatabase().forecastWeatherDao().updateForecastData(wrLong);
            });
            isUpdating.postValue(false);
        }
    }

    private void getCurrentWeatherFromDB() {
        Disposable disposable = repository.getAppDatabase().currentWeatherDao()
                .getCurrentWeather()
                .subscribe(weather -> {
                            if (weather.getMain() != null) {
                                temp.postValue(weather.getMain().getTemp());
                                humidity.postValue(weather.getMain().getHumidity());
                                pressure.postValue(weather.getMain().getPressure());
                            } else {
                                temp.postValue(null);
                                humidity.postValue(null);
                                pressure.postValue(null);
                            }

                            if (weather.getWeather() != null && weather.getWeather().size() > 0 && weather.getWeather().get(0) != null) {
                                weatherIcon.postValue(Utils.getWeatherIcon(repository.getContext(), weather.getWeather().get(0).getIcon()));
                                description.postValue(weather.getWeather().get(0).getMain());
                            } else {
                                weatherIcon.postValue(null);
                                description.postValue(null);
                            }
                            if (weather.getWind() != null) {
                                windDirectIcon.postValue(Utils.getWindDirectionIcon(repository.getContext(), weather.getWind().getDeg()));
                                speed.postValue(weather.getWind().getSpeed());
                            } else {
                                windDirectIcon.postValue(null);
                                speed.postValue(null);
                            }
                            if (weather.getSys() != null) {
                                sunrise.postValue(Utils.getTimeHoursMinutes(repository.getContext(), weather.getSys().getSunrise()));
                                sunset.postValue(Utils.getTimeHoursMinutes(repository.getContext(), weather.getSys().getSunset()));
                            } else {
                                sunrise.postValue(null);
                                sunset.postValue(null);
                            }
                            if (!TextUtils.isEmpty(weather.getName())) {
                                String cityCountry = weather.getName();
                                if (!TextUtils.isEmpty(weather.getSys().getCountry())) {
                                    cityCountry = cityCountry.concat(", " + weather.getSys().getCountry());
                                }
                                cityAndCountry.postValue(cityCountry);
                            }
                            updateTime.postValue(Utils.getUpdateTimeFromLong(repository.getContext(), weather.getTimeOfUpdate()));
                            isUpdating.postValue(false);
                        },
                        e -> Log.d("myLogs", "error"));
        getDisposable().add(disposable);
    }

    private void getTodayWeatherFromDB() {
        Disposable disposable = repository.getAppDatabase().forecastWeatherDao()
                .getWeatherForPeriod(getTimeStartDay(CURRENT_DAY), getTimeStartDay(TOMORROW))
                .subscribe(today -> {
                            todayWeather.postValue(today);
                        },
                        e -> Log.d("myLogs", "error"));
        getDisposable().add(disposable);
    }

    public void getTomorrowWeatherFromDB() {
        Disposable disposable = repository.getAppDatabase().forecastWeatherDao()
                .getWeatherForPeriod(getTimeStartDay(TOMORROW), getTimeStartDay(AFTER_TOMORROW))
                .subscribe(tomorrow -> {
                            tomorrowWeather.postValue(tomorrow);
                        },
                        e -> Log.d("myLogs", "error"));
        getDisposable().add(disposable);
    }

    public void getForecastWeatherFromDB() {
        Disposable disposable = repository.getAppDatabase().forecastWeatherDao()
                .getForecastWeather(getTimeStartDay(CURRENT_DAY))
                .subscribe(forecast -> {
                            java.util.List<WeatherDay> weatherDays = getWeatherDays(forecast);
                            forecastWeather.postValue(weatherDays);
                        },
                        e -> Log.d("myLogs", "error"));
        getDisposable().add(disposable);
    }

    private java.util.List<WeatherDay> getWeatherDays(java.util.List<List> forecast) {
        if (forecast.isEmpty()) {
            return new ArrayList<>();
        }
        java.util.List<WeatherDay> weatherDays = new ArrayList<>();
        java.util.List<List> listWeatherForDay = new ArrayList<>();
        WeatherDay weatherDay = null;
        for (List item : forecast) {
            if ("00:00".equals(Utils.getTimeHoursMinutes(repository.getContext(), item.getDt()))
                    || "01:00".equals(Utils.getTimeHoursMinutes(repository.getContext(), item.getDt()))
                    || "02:00".equals(Utils.getTimeHoursMinutes(repository.getContext(), item.getDt()))) {
                if (!listWeatherForDay.isEmpty() && weatherDay != null) {
                    weatherDay.setListWeatherForDay(listWeatherForDay);
                    weatherDays.add(weatherDay);
                }
                weatherDay = new WeatherDay(repository.getContext(), item.getDt());
                weatherDay.setId(NUMBER_FOR_ID + weatherDays.size());
                listWeatherForDay = new ArrayList<>();
            }
            if (("15:00".equals(Utils.getTimeHoursMinutes(repository.getContext(), item.getDt())) ||
                    "16:00".equals(Utils.getTimeHoursMinutes(repository.getContext(), item.getDt())) ||
                    "17:00".equals(Utils.getTimeHoursMinutes(repository.getContext(), item.getDt()))) && weatherDay != null) {
                if (item.getWeather() != null && !item.getWeather().isEmpty() && item.getWeather().get(0) != null) {
                    weatherDay.setIconDay(Utils.getWeatherIcon(repository.getContext(), item.getWeather().get(0).getIcon()));
                    weatherDay.setDescriptionDay(item.getWeather().get(0).getMain());
                }
                if (item.getMain() != null) {
                    weatherDay.setTemperatureDay(item.getMain().getTemp());
                }
            }
            if (("03:00".equals(Utils.getTimeHoursMinutes(repository.getContext(), item.getDt())) ||
                    "04:00".equals(Utils.getTimeHoursMinutes(repository.getContext(), item.getDt())) ||
                    "05:00".equals(Utils.getTimeHoursMinutes(repository.getContext(), item.getDt()))
            ) && weatherDay != null) {
                if (item.getWeather() != null && !item.getWeather().isEmpty() && item.getWeather().get(0) != null) {
                    weatherDay.setIconNight(Utils.getWeatherIcon(repository.getContext(), item.getWeather().get(0).getIcon()));
                    weatherDay.setDescriptionNight(item.getWeather().get(0).getMain());
                }
                if (item.getMain() != null) {
                    weatherDay.setTemperatureNight(item.getMain().getTemp());
                }
            }
            listWeatherForDay.add(item);
        }
        if (listWeatherForDay.size() == 8 && weatherDay != null) {
            weatherDay.setListWeatherForDay(listWeatherForDay);
            weatherDays.add(weatherDay);
        }
        return weatherDays;
    }

    private Long getTimeStartDay(int period) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        cal.add(Calendar.DATE, period);
        return cal.getTime().getTime() / 1000;
    }

    @Override
    protected void onCleared() {
        super.onCleared();
    }

    void getWeatherFromDB() {
        getCurrentWeatherFromDB();
        getTodayWeatherFromDB();
        getTomorrowWeatherFromDB();
        getForecastWeatherFromDB();
    }

    public void updDB() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                BufferedReader br = null;
                StringBuilder sb = new StringBuilder();
                try {
                    br = new BufferedReader(new InputStreamReader(repository.getContext().getResources().openRawResource(R.raw.city), "UTF-8"));
                    String st;
                    while ((st = br.readLine()) != null) {
                        sb.append(st);
                    }
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    if (br != null) {
                        try {
                            br.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }

                java.util.List<City> citiesFromJson = loadCitiesFromJson(sb.toString());
                repository.getAppDatabase().listCitiesFromFile().insert(citiesFromJson);
            }
        }).start();
    }

    private java.util.List<City> loadCitiesFromJson(String json) {
        Type type = new TypeToken<java.util.List<City>>() {
        }.getType();
        java.util.List<City> cities = new Gson().fromJson(json, type);
        return cities;
    }
}
