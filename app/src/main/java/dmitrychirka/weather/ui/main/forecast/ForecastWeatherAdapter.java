package dmitrychirka.weather.ui.main.forecast;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.h6ah4i.android.widget.advrecyclerview.expandable.RecyclerViewExpandableItemManager;
import com.h6ah4i.android.widget.advrecyclerview.utils.AbstractExpandableItemAdapter;
import com.h6ah4i.android.widget.advrecyclerview.utils.AbstractExpandableItemViewHolder;
import com.h6ah4i.android.widget.advrecyclerview.utils.RecyclerViewAdapterUtils;
import com.h6ah4i.android.widget.advrecyclerview.utils.WrapperAdapterUtils;

import java.util.List;

import dmitrychirka.weather.R;
import dmitrychirka.weather.databinding.RecyclerForecastDayItemBinding;
import dmitrychirka.weather.databinding.RecyclerForecastHoursItemBinding;
import dmitrychirka.weather.model.Utils.Utils;
import dmitrychirka.weather.model.network.response.weatherForecast.WeatherDay;

public class ForecastWeatherAdapter extends AbstractExpandableItemAdapter<ForecastWeatherAdapter.DayViewHolder, ForecastWeatherAdapter.HoursViewHolder>
        implements View.OnClickListener {

    private List<WeatherDay> mItems;
    private RecyclerViewExpandableItemManager manager;
    private int openedGroupPosition = -1;

    ForecastWeatherAdapter(List<WeatherDay> mItems, RecyclerViewExpandableItemManager manager) {
        this.mItems = mItems;
        this.manager = manager;
        setHasStableIds(true);
    }

    @Override
    public int getGroupCount() {
        return mItems.size();
    }

    @Override
    public int getChildCount(int groupPosition) {
        return mItems.get(groupPosition).getListWeatherForDay().size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return mItems.get(groupPosition).getId();
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return mItems.get(groupPosition).getListWeatherForDay().get(childPosition).getId();
    }

    @NonNull
    @Override
    public DayViewHolder onCreateGroupViewHolder(ViewGroup parent, int viewType) {
        RecyclerForecastDayItemBinding binding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.recycler_forecast_day_item, parent, false);
        DayViewHolder dayViewHolder = new DayViewHolder(binding);
        dayViewHolder.binding.dayContainer.setOnClickListener(this);
        return dayViewHolder;
    }

    @NonNull
    @Override
    public HoursViewHolder onCreateChildViewHolder(ViewGroup parent, int viewType) {
        RecyclerForecastHoursItemBinding binding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.recycler_forecast_hours_item, parent, false);
        HoursViewHolder hoursViewHolder = new HoursViewHolder(binding);
        hoursViewHolder.binding.layoutHours.setOnClickListener(v -> {
            if (openedGroupPosition != -1 && manager.isGroupExpanded(openedGroupPosition)) {
                manager.collapseGroup(openedGroupPosition);
                openedGroupPosition = -1;
            }
        });
        return hoursViewHolder;
    }

    @Override
    public void onBindGroupViewHolder(@NonNull ForecastWeatherAdapter.DayViewHolder holder, int groupPosition, int viewType) {
        WeatherDay weatherDay = mItems.get(groupPosition);
        holder.binding.setDayOfWeek(Utils.getDayOfWeekShort(holder.itemView.getContext(), weatherDay.getTimeSeconds()));
        holder.binding.setDate(weatherDay.getDayAndMonth());
        holder.binding.setIconNight(weatherDay.getIconNight());
        holder.binding.setTempNight(Utils.getTemperature(holder.itemView.getContext(), weatherDay.getTemperatureNight()));
        holder.binding.setDescriptionNight(weatherDay.getDescriptionNight());
        holder.binding.setIconDay(weatherDay.getIconDay());
        holder.binding.setTempDay(Utils.getTemperature(holder.itemView.getContext(), weatherDay.getTemperatureDay()));
        holder.binding.setDescriptionDay(weatherDay.getDescriptionDay());
        holder.binding.executePendingBindings();
    }

    @Override
    public void onBindChildViewHolder(@NonNull ForecastWeatherAdapter.HoursViewHolder holder, int groupPosition, int childPosition, int viewType) {
        dmitrychirka.weather.model.network.response.weatherForecast.List weather =
                mItems.get(groupPosition).getListWeatherForDay().get(childPosition);
        if (weather.getMain() != null) {
            holder.binding.setTemp(Utils.getTemperature(holder.itemView.getContext(), weather.getMain().getTemp()));
            holder.binding.setHumidity(weather.getMain().getHumidity());
            holder.binding.setPressure(weather.getMain().getPressure());
        } else {
            holder.binding.setTemp(null);
            holder.binding.setHumidity(null);
            holder.binding.setPressure(null);
        }
        if (weather.getWeather() != null && weather.getWeather().size() > 0 && weather.getWeather().get(0) != null) {
            holder.binding.setWeatherIcon(Utils.getWeatherIcon(holder.itemView.getContext(), weather.getWeather().get(0).getIcon()));
            holder.binding.setDescription(weather.getWeather().get(0).getMain());
        } else {
            holder.binding.setWeatherIcon(null);
            holder.binding.setDescription(null);
        }
        if (weather.getWind() != null) {
            holder.binding.setWindDirectIcon(Utils.getWindDirectionIcon(holder.itemView.getContext(), weather.getWind().getDeg()));
            holder.binding.setWindSpeed(weather.getWind().getSpeed());
        } else {
            holder.binding.setWindDirectIcon(null);
            holder.binding.setWindSpeed(null);
        }
        holder.binding.setTime(Utils.getTimeHoursMinutes(holder.itemView.getContext(), weather.getDt()));
        holder.binding.executePendingBindings();
    }

    @Override
    public boolean onCheckCanExpandOrCollapseGroup(@NonNull DayViewHolder holder, int groupPosition, int x, int y, boolean expand) {
        return true;
    }

    void updateData(List<WeatherDay> list) {
        mItems.clear();
        mItems = list;
        notifyDataSetChanged();
    }

    public class DayViewHolder extends AbstractExpandableItemViewHolder {

        public RecyclerForecastDayItemBinding binding;

        DayViewHolder(RecyclerForecastDayItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    class HoursViewHolder extends AbstractExpandableItemViewHolder {
        RecyclerForecastHoursItemBinding binding;

        HoursViewHolder(RecyclerForecastHoursItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    @Override
    public void onClick(View v) {

        RecyclerView rv = RecyclerViewAdapterUtils.getParentRecyclerView(v);
        if (rv == null) {
            return;
        }
        RecyclerView.ViewHolder vh = rv.findContainingViewHolder(v);

        if (vh == null) {
            return;
        }
        int rootPosition = vh.getAdapterPosition();
        if (rootPosition == RecyclerView.NO_POSITION) {
            return;
        }

        RecyclerView.Adapter rootAdapter = rv.getAdapter();
        if (rootAdapter == null) {
            return;
        }
        int localFlatPosition = WrapperAdapterUtils.unwrapPosition(rootAdapter, this, rootPosition);

        long expandablePosition = manager.getExpandablePosition(localFlatPosition);
        int groupPosition = RecyclerViewExpandableItemManager.getPackedPositionGroup(expandablePosition);
        int childPosition = RecyclerViewExpandableItemManager.getPackedPositionChild(expandablePosition);

        if (childPosition == RecyclerView.NO_POSITION) {
            // Clicked item is a group!
            // toggle expand/collapse
            if (manager.isGroupExpanded(groupPosition)) {
                manager.collapseGroup(groupPosition);
                openedGroupPosition = -1;
            } else {
                if (openedGroupPosition != RecyclerView.NO_POSITION && manager.isGroupExpanded(openedGroupPosition)) {
                    manager.collapseGroup(openedGroupPosition);
                }
                manager.expandGroup(groupPosition);
                openedGroupPosition = groupPosition;
            }
        }
    }
}