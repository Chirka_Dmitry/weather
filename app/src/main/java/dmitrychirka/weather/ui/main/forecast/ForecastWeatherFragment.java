package dmitrychirka.weather.ui.main.forecast;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.h6ah4i.android.widget.advrecyclerview.expandable.RecyclerViewExpandableItemManager;

import java.util.ArrayList;

import dmitrychirka.weather.R;
import dmitrychirka.weather.databinding.FragmentForecastBinding;
import dmitrychirka.weather.model.Utils.SafeLinearLayout;
import dmitrychirka.weather.model.network.response.weatherForecast.WeatherDay;
import dmitrychirka.weather.ui.main.MainActivity;

public class ForecastWeatherFragment extends Fragment {

    public static final String TAG = ForecastWeatherFragment.class.getSimpleName();
    private ForecastWeatherAdapter forecastWeatherAdapter;
    private java.util.List<WeatherDay> weatherDays = new ArrayList<>();
    private FragmentForecastBinding binding;

    public static Fragment startFragment() {
        return new ForecastWeatherFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(
                LayoutInflater.from(getContext()),
                R.layout.fragment_forecast, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initForecastWeatherAdapter();
        if (getActivity() != null) {
            ((MainActivity) getActivity()).getViewModel().getForecastWeather().observe(this, list -> {
                if (list != null) {
                    if (!list.isEmpty()) {
                        binding.tvDateForecast.setText(String.format(getResources().getString(R.string.separator_long_term_dates), list.get(0).getDayAndMonth(), list.get(list.size() - 1).getDayAndMonth()));
                    }
                    updateWeather(list);
                }
            });
        }
        if (getActivity() != null) {
            ((MainActivity) getActivity()).getViewModel().getUpdateDataBinding().observe(this, isUpdate -> {
                if (isUpdate && binding.recyclerView.getAdapter() != null) {
                    binding.recyclerView.getAdapter().notifyDataSetChanged();
                }
            });
        }
    }

    private void initForecastWeatherAdapter() {
        RecyclerViewExpandableItemManager expMgr = new RecyclerViewExpandableItemManager(null);
        forecastWeatherAdapter = new ForecastWeatherAdapter(weatherDays, expMgr);
        binding.recyclerView.setLayoutManager(new SafeLinearLayout(getContext()));
        binding.recyclerView.setAdapter(expMgr.createWrappedAdapter(forecastWeatherAdapter));
    }

    private void updateWeather(java.util.List<WeatherDay> list) {
        forecastWeatherAdapter.updateData(list);
        binding.tvNoData.setVisibility(list.isEmpty() ? View.VISIBLE : View.GONE);
    }
}
