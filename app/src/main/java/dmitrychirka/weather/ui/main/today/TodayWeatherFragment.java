package dmitrychirka.weather.ui.main.today;

import android.content.res.Configuration;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.SimpleItemAnimator;

import java.util.ArrayList;

import dmitrychirka.weather.R;
import dmitrychirka.weather.databinding.FragmentTodayBinding;
import dmitrychirka.weather.model.Utils.Utils;
import dmitrychirka.weather.model.network.response.weatherForecast.List;
import dmitrychirka.weather.ui.main.MainActivity;

public class TodayWeatherFragment extends Fragment {

    private TodayWeatherAdapter todayWeatherAdapter;
    private FragmentTodayBinding binding;

    public static Fragment startFragment() {
        return new TodayWeatherFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(
                LayoutInflater.from(getContext()),
                R.layout.fragment_today, container, false);
        initTodayWeatherAdapter();
        binding.setDay(Utils.getDayOfWeekLong(getContext(), getCurrentTime()));
        binding.setDate(Utils.getDayAndMonth(getContext(), getCurrentTime()));
        if (getActivity() != null) {
            ((MainActivity) getActivity()).getViewModel().getTodayWeather().observe(this, list -> {
                if (list != null) {
                    updateWeather(list);
                }
            });
            ((MainActivity) getActivity()).getViewModel().getUpdateDataBinding().observe(this, isUpdate -> {
                if (isUpdate && binding.recyclerViewToday.getAdapter() != null) {
                    binding.recyclerViewToday.getAdapter().notifyDataSetChanged();
                }
            });
        }
        binding.executePendingBindings();
        return binding.getRoot();
    }

    private long getCurrentTime() {
        return System.currentTimeMillis() / 1000;
    }

    private void initTodayWeatherAdapter() {
        todayWeatherAdapter = new TodayWeatherAdapter(new ArrayList<>());
        int num = (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE ? 2 : 1);
        LinearLayoutManager layoutManager = new GridLayoutManager(getContext(), num);
        binding.recyclerViewToday.setLayoutManager(layoutManager);
        binding.recyclerViewToday.setDrawingCacheEnabled(true);
        binding.recyclerViewToday.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        binding.recyclerViewToday.setAdapter(todayWeatherAdapter);
        if (binding.recyclerViewToday.getItemAnimator() != null) {
            ((SimpleItemAnimator) binding.recyclerViewToday.getItemAnimator()).setSupportsChangeAnimations(false);
        }
    }

    private void updateWeather(java.util.List<List> list) {
        todayWeatherAdapter.updateData(list);
        binding.tvNoData.setVisibility(list.isEmpty() ? View.VISIBLE : View.GONE);
    }
}
