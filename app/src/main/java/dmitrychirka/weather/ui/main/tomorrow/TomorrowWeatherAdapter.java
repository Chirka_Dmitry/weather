package dmitrychirka.weather.ui.main.tomorrow;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import dmitrychirka.weather.R;
import dmitrychirka.weather.databinding.FragmentTodayItemBinding;
import dmitrychirka.weather.model.Utils.Utils;
import dmitrychirka.weather.model.network.response.weatherForecast.List;

public class TomorrowWeatherAdapter extends RecyclerView.Adapter<TomorrowWeatherAdapter.ViewHolder> {

    private java.util.List<List> tomorrowList;

    TomorrowWeatherAdapter(java.util.List<List> tomorrowList) {
        this.tomorrowList = tomorrowList;
    }

    @NonNull
    @Override
    public TomorrowWeatherAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        FragmentTodayItemBinding binding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.fragment_today_item, parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull TomorrowWeatherAdapter.ViewHolder holder, int position) {
        List weather = tomorrowList.get(position);
        if (weather.getMain() != null) {
            holder.binding.setTemp(Utils.getTemperature(holder.itemView.getContext(), weather.getMain().getTemp()));
            holder.binding.setHumidity(weather.getMain().getHumidity());
            holder.binding.setPressure(weather.getMain().getPressure());
        } else {
            holder.binding.setTemp(null);
            holder.binding.setHumidity(null);
            holder.binding.setPressure(null);
        }
        if (weather.getWeather() != null && weather.getWeather().size() > 0 && weather.getWeather().get(0) != null) {
            holder.binding.setWeatherIcon(Utils.getWeatherIcon(holder.itemView.getContext(), weather.getWeather().get(0).getIcon()));
            holder.binding.setDescription(weather.getWeather().get(0).getMain());
        } else {
            holder.binding.setWeatherIcon(null);
            holder.binding.setDescription(null);
        }
        if (weather.getWind() != null) {
            holder.binding.setWindDirectIcon(Utils.getWindDirectionIcon(holder.itemView.getContext(), weather.getWind().getDeg()));
            holder.binding.setWindSpeed(weather.getWind().getSpeed());
        } else {
            holder.binding.setWindDirectIcon(null);
            holder.binding.setWindSpeed(null);
        }
        holder.binding.setTime(Utils.getTimeHoursMinutes(holder.itemView.getContext(), weather.getDt()));
        holder.binding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return tomorrowList.size();
    }

    void updateData(java.util.List<List> list) {
        tomorrowList.clear();
        tomorrowList.addAll(list);
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public FragmentTodayItemBinding binding;

        ViewHolder(FragmentTodayItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
