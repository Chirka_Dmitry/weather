package dmitrychirka.weather.ui.main.tomorrow;

import android.content.res.Configuration;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.SimpleItemAnimator;

import java.util.ArrayList;

import dmitrychirka.weather.R;
import dmitrychirka.weather.databinding.FragmentTomorrowBinding;
import dmitrychirka.weather.model.Utils.Utils;
import dmitrychirka.weather.model.network.response.weatherForecast.List;
import dmitrychirka.weather.ui.main.MainActivity;

public class TomorrowWeatherFragment extends Fragment {

    private TomorrowWeatherAdapter tomorrowWeatherAdapter;
    private FragmentTomorrowBinding binding;

    public static Fragment startFragment() {
        return new TomorrowWeatherFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(
                LayoutInflater.from(getContext()),
                R.layout.fragment_tomorrow, container, false);
        initTomorrowWeatherAdapter();
        binding.setDay(Utils.getDayOfWeekLong(getContext(), Utils.getTomorrowStartSeconds()));
        binding.setDate(Utils.getDayAndMonth(getContext(), Utils.getTomorrowStartSeconds()));
        if (getActivity() != null) {
            ((MainActivity) getActivity()).getViewModel().getTomorrowWeather().observe(this, list -> {
                if (list != null) {
                    updateWeather(list);
                }
            });
            ((MainActivity) getActivity()).getViewModel().getUpdateDataBinding().observe(this, isUpdate -> {
                if (isUpdate && binding.recyclerViewTomorrow.getAdapter() != null) {
                    binding.recyclerViewTomorrow.getAdapter().notifyDataSetChanged();
                }
            });
        }
        binding.executePendingBindings();
        return binding.getRoot();
    }

    private void initTomorrowWeatherAdapter() {
        tomorrowWeatherAdapter = new TomorrowWeatherAdapter(new ArrayList<>());
        int num = (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE ? 2 : 1);
        LinearLayoutManager layoutManager = new GridLayoutManager(getContext(), num);
        binding.recyclerViewTomorrow.setLayoutManager(layoutManager);
        binding.recyclerViewTomorrow.setDrawingCacheEnabled(true);
        binding.recyclerViewTomorrow.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        binding.recyclerViewTomorrow.setAdapter(tomorrowWeatherAdapter);
        if (binding.recyclerViewTomorrow.getItemAnimator() != null) {
            ((SimpleItemAnimator) binding.recyclerViewTomorrow.getItemAnimator()).setSupportsChangeAnimations(false);
        }
    }

    private void updateWeather(java.util.List<List> list) {
        tomorrowWeatherAdapter.updateData(list);
        binding.tvNoData.setVisibility(list.isEmpty() ? View.VISIBLE : View.GONE);
    }
}
