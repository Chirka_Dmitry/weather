package dmitrychirka.weather.ui.search;

import android.app.Dialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.SearchView;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SimpleItemAnimator;

import com.google.android.material.appbar.AppBarLayout;

import java.lang.ref.WeakReference;

import javax.inject.Inject;

import dmitrychirka.weather.R;
import dmitrychirka.weather.databinding.ActivitySearchBinding;
import dmitrychirka.weather.di.App;
import dmitrychirka.weather.di.FactoryViewModel;
import dmitrychirka.weather.model.Utils.ShPrUtils;
import dmitrychirka.weather.model.Utils.Utils;
import dmitrychirka.weather.model.network.response.cities.City;
import dmitrychirka.weather.ui.base.BaseActivity;
import dmitrychirka.weather.ui.search.adapters.CountryAdapter;
import dmitrychirka.weather.ui.search.fragments.SearchFragment;

public class SearchActivity extends BaseActivity implements AppBarLayout.OnOffsetChangedListener {

    private String query;
    private Dialog dialog;
    private ActivitySearchBinding binding;
    private SearchActivityViewModel searchViewModel;
    private static final String SEARCH_STRING = "search_string";

    @Inject
    FactoryViewModel viewModelFactory;

    public SearchActivityViewModel getSearchViewModel() {
        return searchViewModel;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ((App) getApplication()).getAppComponent().inject(this);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_search);
        searchViewModel = ViewModelProviders.of(this, viewModelFactory)
                .get(SearchActivityViewModel.class);

        if (savedInstanceState == null) {
            searchViewModel.setCountryShort(ShPrUtils.getCountryShort(this));
            searchViewModel.setCountryLong(ShPrUtils.getCountryLong(this));
            searchViewModel.setCountryFlag(ShPrUtils.getCountryFlag(this));
        }
        initActionBar();
        initSearchView();
        initCurrCountry();
        createSearchFragment();
        binding.layoutCountry.setOnClickListener(v -> createCountryDialog());
        binding.appBar.setOnClickListener(v -> binding.appBar.setExpanded(false));
    }

    private void createSearchFragment() {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_layout, SearchFragment.startFragment(), SearchFragment.TAG)
                .commit();
    }

    private void initCurrCountry() {
        binding.setCountryName(searchViewModel.getCountryLong());
        binding.setCountryFlag(this.getDrawable(searchViewModel.getCountryFlag()));
    }

    private void initActionBar() {
        setSupportActionBar(binding.toolbar);
        binding.appBar.addOnOffsetChangedListener(this);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(R.string.settings);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    private void initSearchView() {
        binding.searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                searchViewModel.findCity(newText);
                return false;
            }
        });
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        binding.searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
        if (Math.abs(verticalOffset) == appBarLayout.getTotalScrollRange()) {
            // Collapsed
            showSearchView();
        } else if (verticalOffset == 0) {
            hideSearchView();
            // Expanded
        } else {
            // Somewhere in between
            showActivityTitle();
            showActivityImage();
        }
    }

    private void showActivityTitle() {
        binding.setActivityTitleVisibility(true);
    }

    private void showActivityImage() {
        binding.setParallaxImgVisibility(true);
    }

    private void showSearchView() {
        binding.setSearchViewVisibility(true);
        binding.searchView.onActionViewExpanded();
        if (!TextUtils.isEmpty(query)) {
            binding.searchView.setQuery(query, false);
            query = null;
        }
        if (binding.searchView.hasFocus()) {
            showKeyboard(binding.searchView.findFocus());
        }
    }

    private void showKeyboard(View view) {
        Utils.showKeyboard(getBaseContext(), view);
    }

    private void hideSearchView() {
        binding.setSearchViewVisibility(false);
        binding.searchView.requestFocusFromTouch();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            binding.searchView.setQuery(query, false);
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState != null)
            query = savedInstanceState.getString(SEARCH_STRING);
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(SEARCH_STRING, binding.searchView.getQuery().toString());
    }

    public void createCountryDialog() {
        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (dialog.getWindow() != null) {
            dialog.getWindow().setContentView(R.layout.search_picker_dialog);
        }
        CountryAdapter adapter = initDialogAdapter();
        initDialogRecycler(adapter);
        initDialogSearch(adapter);
        dialog.show();
    }

    private void initDialogSearch(CountryAdapter adapter) {
        if (dialog.getWindow() != null) {
            ImageView imgClearDialog = dialog.getWindow().findViewById(R.id.iv_clear);
            EditText searchDialog = dialog.getWindow().findViewById(R.id.et_search);
            searchDialog.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                }

                @Override
                public void afterTextChanged(Editable s) {
                    adapter.getFilter().filter(s);
                    imgClearDialog.setVisibility(TextUtils.isEmpty(s) ? View.GONE : View.VISIBLE);
                }
            });

            imgClearDialog.setOnClickListener(v -> {
                if (!TextUtils.isEmpty(searchDialog.getText())) {
                    searchDialog.setText("");
                }
            });
        }
    }

    private CountryAdapter initDialogAdapter() {
        CountryAdapter adapter = new CountryAdapter(new WeakReference<>(this), Utils.getCountriesEnglish());
        adapter.setClickListener(v -> {
            CountryAdapter.ViewHolder viewHolder = (CountryAdapter.ViewHolder) v.getTag();
            searchViewModel.setCountryShort(adapter.getItemsFiltered().get(viewHolder.getAdapterPosition()).getShortName().toUpperCase());
            searchViewModel.setCountryLong(adapter.getItemsFiltered().get(viewHolder.getAdapterPosition()).getLongName());
            searchViewModel.setCountryFlag(adapter.getItemsFiltered().get(viewHolder.getAdapterPosition()).getFlag());
            searchViewModel.findCity("");
            binding.searchView.setQuery("", false);
            initCurrCountry();
            binding.executePendingBindings();
            dialog.dismiss();
        });
        return adapter;
    }

    private void initDialogRecycler(CountryAdapter adapter) {
        if (dialog.getWindow() != null) {
            RecyclerView recyclerView = dialog.getWindow().findViewById(R.id.recycler_countryDialog);
            LinearLayoutManager layoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
            recyclerView.setLayoutManager(layoutManager);
            recyclerView.setHasFixedSize(true);
            recyclerView.setDrawingCacheEnabled(true);
            recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
            recyclerView.setAdapter(adapter);
            if (recyclerView.getItemAnimator() != null) {
                ((SimpleItemAnimator) recyclerView.getItemAnimator()).setSupportsChangeAnimations(false);
            }
        }
    }

    public void setDialogNoTextVisible(boolean visible) {
        if (dialog != null && dialog.getWindow() != null) {
            dialog.getWindow().findViewById(R.id.tv_no_results).setVisibility(visible ? View.VISIBLE : View.GONE);
        }
    }

    public void setActivityResults(City city) {
        ShPrUtils.updateCity(this, city.getId().toString(), searchViewModel.getCountryShort()
                , searchViewModel.getCountryLong(), String.valueOf(city.getCoord().getLat())
                , String.valueOf(city.getCoord().getLon()), searchViewModel.getCountryFlag());
        setResult(RESULT_OK);
        finish();
    }
}