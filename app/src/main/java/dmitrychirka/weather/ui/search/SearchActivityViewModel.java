package dmitrychirka.weather.ui.search;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import javax.inject.Inject;

import dmitrychirka.weather.model.Repository;
import dmitrychirka.weather.model.network.response.cities.City;
import dmitrychirka.weather.ui.base.BaseViewModel;
import io.reactivex.disposables.Disposable;

public class SearchActivityViewModel extends BaseViewModel {

    private Repository repository;
    private String currCountryLong;
    private String currCountryShort;
    private int currCountryFlag;
    private MutableLiveData<java.util.List<City>> cities = new MutableLiveData<>();

    @Inject
    SearchActivityViewModel(Repository repository) {
        this.repository = repository;
    }

    public MutableLiveData<java.util.List<City>> getCities() {
        return cities;
    }

    void findCity(String city) {
        getDisposable().clear();
        Disposable disposable = repository.getAppDatabase()
                .listCitiesFromFile().getCitiesFromDB(city, currCountryShort)
                .subscribe(listCity -> this.cities.postValue(listCity),
                        e -> Log.d("myLogs", "error"));
        getDisposable().add(disposable);
    }

    void setCountryShort(String countryShort) {
        currCountryShort = countryShort;
    }

    void setCountryLong(String countryLong) {
        currCountryLong = countryLong;
    }

    String getCountryLong() {
        return currCountryLong;
    }

    String getCountryShort() {
        return currCountryShort;
    }

    void setCountryFlag(int countryFlag) {
        currCountryFlag = countryFlag;
    }

    int getCountryFlag() {
        return currCountryFlag;
    }

    @Override
    protected void onCleared() {
        super.onCleared();
    }
}
