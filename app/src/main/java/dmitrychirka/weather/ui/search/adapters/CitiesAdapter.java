package dmitrychirka.weather.ui.search.adapters;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import dmitrychirka.weather.R;
import dmitrychirka.weather.databinding.RecyclerCityItemBinding;
import dmitrychirka.weather.model.network.response.cities.City;
import dmitrychirka.weather.ui.search.SearchActivity;

public class CitiesAdapter extends RecyclerView.Adapter<CitiesAdapter.ViewHolder> {

    private List<City> cities;

    public CitiesAdapter(List<City> cities) {
        this.cities = cities;
        setHasStableIds(true);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater =
                LayoutInflater.from(parent.getContext());
        RecyclerCityItemBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.recycler_city_item, parent, false);
        binding.getRoot().setOnClickListener(v -> ((SearchActivity) v.getContext())
                .setActivityResults(cities.get((Integer) binding.getRoot().getTag())));
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        City item = cities.get(position);
        holder.itemView.setTag(position);
        holder.itemBinding.setCity(item.getName());
        holder.itemBinding.setCountry(item.getCountry());
        holder.itemBinding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return cities.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public void swapItems(List<City> cities) {
        this.cities.clear();
        this.cities = cities;
        notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private final RecyclerCityItemBinding itemBinding;

        ViewHolder(final RecyclerCityItemBinding itemBinding) {
            super(itemBinding.getRoot());
            this.itemBinding = itemBinding;
        }
    }
}