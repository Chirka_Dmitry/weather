package dmitrychirka.weather.ui.search.adapters;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import dmitrychirka.weather.R;
import dmitrychirka.weather.databinding.RecyclerCountryItemBinding;
import dmitrychirka.weather.model.Country;
import dmitrychirka.weather.ui.search.SearchActivity;

public class CountryAdapter extends RecyclerView.Adapter<CountryAdapter.ViewHolder> implements Filterable {

    private List<Country> countries;

    public List<Country> getItemsFiltered() {
        return itemsFiltered;
    }

    private List<Country> itemsFiltered;

    private View.OnClickListener clickListener;
    private WeakReference<SearchActivity> searchActivity;

    public void setClickListener(View.OnClickListener listener) {
        clickListener = listener;
    }

    public CountryAdapter(WeakReference<SearchActivity> searchActivity, List<Country> countries) {
        this.countries = countries;
        itemsFiltered = countries;
        this.searchActivity = searchActivity;
        setHasStableIds(true);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater =
                LayoutInflater.from(parent.getContext());
        RecyclerCountryItemBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.recycler_country_item, parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Country country = itemsFiltered.get(position);
        holder.binding.setLongName(country.getLongName());
        holder.binding.setFlag(holder.itemView.getContext().getDrawable(country.getFlag()));
    }

    @Override
    public int getItemCount() {
        return itemsFiltered.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private final RecyclerCountryItemBinding binding;

        public ViewHolder(final RecyclerCountryItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            binding.getRoot().setTag(this);
            binding.getRoot().setOnClickListener(clickListener);
        }
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence text) {
                List<Country> filtered = new ArrayList<>();
                if (TextUtils.isEmpty(text)) {
                    filtered = countries;
                } else {
                    for (Country item : countries) {
                        if (item.getLongName().toLowerCase().startsWith(text.toString().toLowerCase())) {
                            filtered.add(item);
                        }
                    }
                }
                FilterResults results = new FilterResults();
                results.count = filtered.size();
                results.values = filtered;
                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                itemsFiltered = (ArrayList<Country>) results.values;
                notifyDataSetChanged();
                searchActivity.get().setDialogNoTextVisible(itemsFiltered.isEmpty());
            }
        };
    }
}