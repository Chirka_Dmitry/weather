package dmitrychirka.weather.ui.search.fragments;

import android.content.res.Configuration;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.SimpleItemAnimator;

import java.util.ArrayList;

import dmitrychirka.weather.R;
import dmitrychirka.weather.databinding.FragmentSearchBinding;
import dmitrychirka.weather.ui.search.SearchActivity;
import dmitrychirka.weather.ui.search.adapters.CitiesAdapter;

public class SearchFragment extends Fragment {

    public static final String TAG = "SearchFragment";
    private CitiesAdapter adapter;
    private FragmentSearchBinding binding;

    public static Fragment startFragment() {
        return new SearchFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(LayoutInflater.from(getContext()),
                R.layout.fragment_search, container, false);
        initAdapter();
        if (getActivity() != null) {
            ((SearchActivity) getActivity()).getSearchViewModel().getCities().observe(this, cities -> {
                if (cities != null) {
                    binding.setNoCityVisibility(cities.isEmpty());
                    adapter.swapItems(cities);
                }
            });
        }
        return binding.getRoot();
    }

    private void initAdapter() {
        adapter = new CitiesAdapter(new ArrayList<>());
        int num = (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE ? 2 : 1);
        LinearLayoutManager layoutManager = new GridLayoutManager(getContext(), num);
        binding.recyclerCity.setLayoutManager(layoutManager);
        binding.recyclerCity.setHasFixedSize(true);
        binding.recyclerCity.setDrawingCacheEnabled(true);
        binding.recyclerCity.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        binding.recyclerCity.setAdapter(adapter);
        if (binding.recyclerCity.getItemAnimator() != null) {
            ((SimpleItemAnimator) binding.recyclerCity.getItemAnimator()).setSupportsChangeAnimations(false);
        }
    }
}