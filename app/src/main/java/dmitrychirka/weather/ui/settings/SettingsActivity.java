package dmitrychirka.weather.ui.settings;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.MenuItem;

import androidx.appcompat.app.AppCompatActivity;

import dmitrychirka.weather.R;
import dmitrychirka.weather.model.Utils.LocaleManager;
import dmitrychirka.weather.model.Utils.ShPrUtils;
import dmitrychirka.weather.ui.base.BaseActivity;

public class SettingsActivity extends BaseActivity {
    private SharedPreferences.OnSharedPreferenceChangeListener prefListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(R.string.settings);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        getSupportFragmentManager().beginTransaction().replace(android.R.id.content, new SettingsFragment()).commit();
        AppCompatActivity context = this;

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        prefListener = (sharedPreferences, key) -> {
            if (key.equals(ShPrUtils.LANGUAGE_KEY)) {
                setNewLocale(context, ShPrUtils.getLanguagePref(context));
                ShPrUtils.setLocaleChanged(getBaseContext(), true);
            }
        };
        prefs.registerOnSharedPreferenceChangeListener(prefListener);
    }

    private void setNewLocale(AppCompatActivity mContext, @LocaleManager.LocaleDef String language) {
        LocaleManager.setNewLocale(this, language);
        finish();
        overridePendingTransition(0, 0);
        startActivity(getIntent().addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP));
        overridePendingTransition(0, 0);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }
}
