package dmitrychirka.weather.ui.settings;


import android.os.Bundle;

import androidx.preference.PreferenceFragmentCompat;

import dmitrychirka.weather.R;


public class SettingsFragment extends PreferenceFragmentCompat {

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        setPreferencesFromResource(R.xml.preferences, rootKey);
    }
}
